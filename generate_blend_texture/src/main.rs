use image::Rgba;

fn main() {
    let size = 128;

    let v0 = glam::vec2(0.0, 0.0);
    let v1 = glam::vec2(1.0, 0.0);
    let v2 = glam::vec2(0.5, 1.0);
    let v3 = glam::vec2(1.5, 1.0);

    let r = Rgba::<u8>([255u8, 0, 0, 0]);
    let g = Rgba::<u8>([0u8, 255, 0, 0]);
    let b = Rgba::<u8>([0u8, 0, 255, 0]);
    let a = Rgba::<u8>([0u8, 0, 0, 255]);

    let i = image::ImageBuffer::from_fn(size, size, |x, y| {
        let y = y as f32 / size as f32;

        let x = (x as f32 / size as f32) + (y / 2.0);

        let p = glam::vec2(x, y);

        let d0 = (p - v0).length_squared();
        let d1 = (p - v1).length_squared();
        let d2 = (p - v2).length_squared();
        let d3 = (p - v3).length_squared();

        if d0 < d1 && d0 < d2 && d0 < d3 {
            r
        } else if d1 < d2 && d1 < d3 {
            g
        } else if d2 < d3 {
            b
        } else {
            a
        }
    });

    i.save_with_format("colonists/assets/blend.png", image::ImageFormat::Png)
        .unwrap();
}
