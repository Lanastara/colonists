# Changelog
All notable changes to this project will be documented in this file.

## [unreleased]

### Documentation

- Added readme and changelog

### Features

- Allow configuring texture mapping via json file

