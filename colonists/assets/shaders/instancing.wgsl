#import bevy_pbr::mesh_types
#import bevy_pbr::mesh_view_bindings

fn mesh_position_world_to_clip(world_position: vec4<f32>) -> vec4<f32> {
    return view.view_proj * world_position;
}

struct VertexInput {
    @location(0) pos: vec3<f32>,
    @location(3) offset: vec3<f32>,
    @location(4) uv_min: vec2<f32>,
    @location(5) uv_max: vec2<f32>,
    @location(6) scale: vec2<f32>,
    @location(7) color: vec4<f32>,
};


@group(1) @binding(0)
var texture: texture_2d<f32>;
@group(1) @binding(1)
var texture_sampler: sampler;

@vertex
fn vertex(input: VertexInput) -> VertexOutput {
    var out: VertexOutput;
    var scaled = vec3(input.pos.xy * input.scale, 0.0);
    var anchor = vec3(0.0, 0.5, 0.0);
    var clip = ((scaled + input.offset) + anchor);
    out.clip_position = mesh_position_world_to_clip(vec4(clip, 1.0));
    out.uv = (input.uv_max-input.uv_min)*(input.pos.xy+ vec2(0.5,0.5))+input.uv_min;
    return out;
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) uv: vec2<f32>,
};

@fragment
fn fragment(input: VertexOutput) -> @location(0) vec4<f32> {
    let color = textureSample(texture, texture_sampler, input.uv);
    if(color.a) == 0.0 {
        discard;
    }
    return color;
}