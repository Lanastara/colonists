use bevy::{
    app::AppExit,
    prelude::{
        resource_changed, BuildChildren, Button, ButtonBundle, Color, Commands, Component,
        EventWriter, IntoSystemConfig, NextState, NodeBundle, Plugin, Query, Res, ResMut, State,
        States, Visibility, With,
    },
    ui::{AlignItems, FlexDirection, Interaction, Size, Style, UiRect, Val},
};

pub struct GameMenuPlugin;

impl Plugin for GameMenuPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_state::<GameMenuState>()
            .add_startup_system(setup_menu)
            .add_system(handle_buttons)
            .add_system(update_menu.run_if(resource_changed::<State<GameMenuState>>()));
    }
}

#[derive(Debug, Component)]
struct Menu;

#[derive(Debug, Component)]
enum ButtonCommand {
    Continue,
    Quit,
}

fn handle_buttons(
    interactions: Query<(&Interaction, &ButtonCommand), With<Button>>,
    mut state: ResMut<NextState<GameMenuState>>,
    mut app_exit_events: EventWriter<AppExit>,
) {
    for (interaction, command) in interactions.iter() {
        if let Interaction::Clicked = interaction {
            match command {
                ButtonCommand::Continue => {
                    state.set(GameMenuState::Closed);
                }
                ButtonCommand::Quit => app_exit_events.send(AppExit),
            }
        }
    }
}

fn update_menu(state: Res<State<GameMenuState>>, mut query: Query<&mut Visibility, With<Menu>>) {
    *query.single_mut() = match state.0 {
        GameMenuState::Open => Visibility::Visible,
        GameMenuState::Closed => Visibility::Hidden,
    };
}

fn setup_menu(mut commands: Commands) {
    commands
        .spawn((
            NodeBundle {
                visibility: Visibility::Hidden,
                style: Style {
                    size: Size::all(Val::Percent(100.0)),
                    justify_content: bevy::ui::JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..Default::default()
                },
                ..Default::default()
            },
            Menu,
        ))
        .with_children(|parent| {
            parent
                .spawn(NodeBundle {
                    style: Style {
                        size: Size::new(Val::Px(400.0), Val::Auto),
                        flex_direction: FlexDirection::Column,
                        align_items: AlignItems::Center,
                        padding: UiRect::all(Val::Px(10.0)),
                        ..Default::default()
                    },
                    background_color: Color::GRAY.into(),
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent.spawn((
                        ButtonBundle {
                            style: Style {
                                size: Size::new(Val::Percent(100.0), Val::Px(100.0)),
                                margin: UiRect::all(Val::Px(10.0)),
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                        ButtonCommand::Continue,
                    ));

                    parent.spawn((
                        ButtonBundle {
                            style: Style {
                                size: Size::new(Val::Percent(100.0), Val::Px(100.0)),
                                margin: UiRect::all(Val::Px(10.0)),
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                        ButtonCommand::Quit,
                    ));
                });
        });
}

#[derive(Debug, Default, States, Hash, PartialEq, Eq, Clone)]
pub enum GameMenuState {
    Open,
    #[default]
    Closed,
}
