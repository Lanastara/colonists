use bevy::prelude::*;

pub struct PathFinished(pub Entity);
pub struct PathSegmentStarted(pub Entity, pub PathSegment);

pub struct PathingPlugin(pub f32);

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemSet)]
pub enum PathingSet {
    PreUpdate,
    Update,
    Flush,
}

impl Plugin for PathingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(PathingSpeed(self.0))
            .add_event::<PathFinished>()
            .add_event::<PathSegmentStarted>()
            .configure_set(PathingSet::PreUpdate.before(PathingSet::Update))
            .configure_set(PathingSet::Update.before(PathingSet::Flush));
    }
}

#[derive(Debug, Resource)]
pub struct PathingSpeed(f32);

pub fn update_pathing(
    mut items: Query<(Entity, &mut Path)>,
    mut path_finished: EventWriter<PathFinished>,
    mut segment_started: EventWriter<PathSegmentStarted>,
    time: Res<Time>,
    speed: Res<PathingSpeed>,
) {
    let d_time = time.delta_seconds() * speed.0;
    for (entity, mut path) in items.iter_mut() {
        match path.advance_by(d_time) {
            Some(PathEvent::PathFinished) => path_finished.send(PathFinished(entity)),
            Some(PathEvent::PathSegmentStarted(segment)) => {
                segment_started.send(PathSegmentStarted(entity, segment))
            }
            None => {}
        }
    }
}

enum PathEvent {
    PathFinished,
    PathSegmentStarted(PathSegment),
}

#[derive(Debug, Component)]
pub struct Path {
    segments: Vec<PathSegment>,
    position: f32,
}

impl Path {
    pub fn new(segments: Vec<PathSegment>) -> Self {
        Self {
            segments,
            position: 0.0,
        }
    }

    fn advance_by(&mut self, d_time: f32) -> Option<PathEvent> {
        let segment = self.index();
        self.position += d_time;
        let new_segment = self.index();
        if segment != new_segment {
            if let Some(segment) = self.segments.get(new_segment) {
                Some(PathEvent::PathSegmentStarted(segment.clone()))
            } else {
                Some(PathEvent::PathFinished)
            }
        } else {
            None
        }
    }

    fn index(&self) -> usize {
        self.position as usize
    }

    pub fn current_segment(&self) -> Option<PathSegment> {
        let index = self.index();
        self.segments.get(index).cloned()
    }

    pub fn current(&self) -> Option<(Entity, Entity, f32)> {
        let index = self.index();
        let segment = self.segments.get(index)?;
        Some((segment.from, segment.to, self.position % 1.0))
    }
}

#[derive(Debug, Clone)]
pub struct PathSegment {
    pub from: Entity,
    pub to: Entity,
}
