use bevy::prelude::Resource;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Default, Resource)]
pub struct Config {
    pub graphics: GraphicConfig,
    pub misc: MiscConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MiscConfig {
    pub capture_mouse: bool,
}

impl Default for MiscConfig {
    fn default() -> Self {
        Self {
            capture_mouse: true,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GraphicConfig {
    pub display: Display,
    pub v_sync: bool,
}

impl Default for GraphicConfig {
    fn default() -> Self {
        Self {
            display: Display::default(),
            v_sync: true,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub enum Display {
    #[default]
    BorderlessFusscreen,
    Fullscreen,
    Windowed {
        width: f32,
        height: f32,
    },
}
