#![allow(clippy::type_complexity)]
#![allow(clippy::too_many_arguments)]
use std::collections::{BTreeMap, HashMap};

use animation::{update_animations, Animation, AnimationBuilder, AnimationPosition};
use bevy::{
    app::AppExit,
    input::mouse::MouseWheel,
    prelude::*,
    window::{Cursor, CursorGrabMode, PrimaryWindow, WindowResolution},
};
use colonists_terrain::{
    get_world_height, get_world_position, prelude::*, TerrainMap, TerrainPosition,
};
use game_menu::GameMenuState;
use pathing::{update_pathing, Path, PathSegmentStarted, PathingSet};
use sprite3d::{SpriteSheet3DBundle, TextureAtlasSprite3D};

mod animation;
mod config;
mod game_menu;
mod pathing;
mod sprite3d;

fn load_config() -> config::Config {
    let Ok(file) = std::fs::File::open("settings.yml")else {
          return Default::default();
    };
    serde_yaml::from_reader(file).unwrap_or_default()
}
fn save_config(config: &config::Config) {
    let file = std::fs::File::create("settings.yml").unwrap();
    serde_yaml::to_writer(file, config).unwrap();
}

fn main() {
    let config = load_config();
    let mut app = App::new();
    let mut cursor = Cursor::default();
    if config.misc.capture_mouse {
        cursor.grab_mode = CursorGrabMode::Confined;
    }
    let window = Window {
        present_mode: if config.graphics.v_sync {
            bevy::window::PresentMode::AutoVsync
        } else {
            bevy::window::PresentMode::AutoNoVsync
        },
        mode: match config.graphics.display {
            config::Display::BorderlessFusscreen => bevy::window::WindowMode::BorderlessFullscreen,
            config::Display::Fullscreen => bevy::window::WindowMode::Fullscreen,
            config::Display::Windowed { .. } => bevy::window::WindowMode::Windowed,
        },
        cursor,
        resolution: if let config::Display::Windowed { width, height } = config.graphics.display {
            WindowResolution::new(width, height)
        } else {
            WindowResolution::default()
        },
        ..Default::default()
    };
    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(window),
        ..Default::default()
    }));

    // #[cfg(debug_assertions)]
    app.add_plugin(bevy_screen_diagnostics::ScreenDiagnosticsPlugin::default())
        .add_plugin(bevy_screen_diagnostics::ScreenFrameDiagnosticsPlugin);

    #[cfg(debug_assertions)]
    app.add_plugin(TerrainPlugin {
        chunk: None,
        world: (10, 10),
    });
    #[cfg(not(debug_assertions))]
    app.add_plugin(TerrainPlugin {
        chunk: Some((10, 10)),
        world: (1_000, 1_000),
    });

    app.add_plugin(pathing::PathingPlugin(1.0))
        .add_plugin(sprite3d::Sprite3DPlugin)
        .add_plugin(game_menu::GameMenuPlugin)
        .add_plugin(animation::AnimationPlugin::<AnimationData, AnimationEvent>::default())
        .insert_resource(config)
        .init_resource::<Selected>()
        .init_resource::<ColonistAtlas>()
        .init_resource::<AnimationSpriteHandles>()
        .init_resource::<AnimationCatalogue>()
        .add_state::<AppState>()
        .add_system(load_textures.in_schedule(OnEnter(AppState::Startup)))
        .add_system(check_textures.in_set(OnUpdate(AppState::Startup)))
        .add_system(setup_colonists.in_schedule(OnExit(AppState::Startup)))
        .add_systems(
            (setup, create_animations, spawn_colonist).in_schedule(OnEnter(AppState::Running)),
        )
        .add_systems(
            (
                input,
                update_animations::<AnimationData, AnimationEvent>,
                update_pathing.in_set(PathingSet::PreUpdate),
            )
                .in_set(OnUpdate(AppState::Running))
                .in_set(OnUpdate(GameMenuState::Closed)),
        )
        .add_systems(
            (update_pathing_position, update_carrier_animation).in_set(pathing::PathingSet::Update),
        )
        .add_system(close.in_base_set(CoreSet::LastFlush))
        .run();
}

fn close(event: EventReader<AppExit>, settings: Res<config::Config>) {
    if !event.is_empty() {
        save_config(&settings);
    }
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash, States)]
enum AppState {
    #[default]
    Startup,
    Running,
}

fn load_textures(
    mut animation_sprite_handles: ResMut<AnimationSpriteHandles>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
) {
    animation_sprite_handles.handles = asset_server.load_folder("textures/animations").unwrap();
    commands.insert_resource(TerrainTextureLoader::new(
        asset_server.load("terrain.png"),
        asset_server.load("blend.png"),
        4,
    ));
}

fn check_textures(
    mut next_state: ResMut<NextState<AppState>>,
    animation_sprite_handles: ResMut<AnimationSpriteHandles>,
    asset_server: Res<AssetServer>,
) {
    if let bevy::asset::LoadState::Loaded = asset_server.get_group_load_state(
        animation_sprite_handles
            .handles
            .iter()
            .map(|handle| handle.id()),
    ) {
        next_state.set(AppState::Running);
    }
}

#[derive(Debug, Resource, Default)]
struct AnimationCatalogue {
    characters: HashMap<String, CharacterAnimationCatalogue>,
}

impl AnimationCatalogue {
    fn get_animation(
        &self,
        character_name: impl Into<String>,
        animation_name: impl Into<String>,
    ) -> Option<(
        Handle<Animation<AnimationData, AnimationEvent>>,
        AnimationPosition,
    )> {
        self.characters
            .get(&character_name.into())?
            .animations
            .get(&animation_name.into())
            .cloned()
    }
}

type AnimationData = TextureAtlasSprite3D;

type AnimationEvent = ();

#[derive(Debug, Default)]
struct CharacterAnimationCatalogue {
    animations: HashMap<
        String,
        (
            Handle<Animation<AnimationData, AnimationEvent>>,
            AnimationPosition,
        ),
    >,
}

fn create_animations(
    mut animation_catalogue: ResMut<AnimationCatalogue>,
    animation_sprite_handles: Res<AnimationSpriteHandles>,
    mut animations: ResMut<Assets<Animation<AnimationData, AnimationEvent>>>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    asset_server: Res<AssetServer>,
    atlas: Res<ColonistAtlas>,
) {
    let mut tmp = HashMap::<String, HashMap<String, BTreeMap<String, usize>>>::new();

    let Some(atlas) = texture_atlases.get(dbg!(&atlas.0)) else {
        warn!("Could not retrieve Texture Atlas");
        return;
    };

    for handle in &animation_sprite_handles.handles {
        let Some(path) = asset_server.get_handle_path(handle) else {
            warn!(?handle, "No path for animation asset.");
            continue;
        };

        let Ok(path) = path.path().strip_prefix("textures/animations") else {
            warn!(?path, "Could not strip path.");
            continue;
        };

        let parts: Vec<_> = path.iter().collect();
        let Some(character_name) = parts.get(0) else {
            warn!(?path, "Could not extract character name");
            continue;
        };
        let Some(animation_name) = parts.get(1) else {
            warn!(?path, "Could not extract animation name");
            continue;
        };
        let Some(frame_name) = parts.get(2) else {
            warn!(?path, "Could not extract frame name");
            continue;
        };
        let Some(character_name) = character_name.to_str().map(|s| s.to_string())else {
            warn!(?character_name, "Could not convert Character name to string.");
          continue;
        };
        let Some(animation_name) = animation_name.to_str().map(|s| s.to_string())else {
            warn!(?animation_name, "Could not convert Animation name to string.");
          continue;
        };
        let Some(frame_name) = frame_name.to_str().map(|s| s.to_string())else {
            warn!(?frame_name, "Could not convert Frame name to string.");
          continue;
        };

        let Some(index) = atlas.get_texture_index(&handle.typed_weak()) else {
            warn!(?handle, ?path, "Could not retrieve TextureAtlas index.");
            continue;
        };

        fn add_to_character(
            character_catalogue: &mut HashMap<String, BTreeMap<String, usize>>,
            animation_name: String,
            frame_name: String,
            index: usize,
        ) {
            match character_catalogue.get_mut(&animation_name) {
                Some(animation) => {
                    animation.insert(frame_name, index);
                }
                None => {
                    let mut animation = BTreeMap::new();

                    animation.insert(frame_name, index);
                    character_catalogue.insert(animation_name, animation);
                }
            }
        }

        match tmp.get_mut(&character_name) {
            Some(character) => {
                add_to_character(character, animation_name, frame_name, index);
            }
            None => {
                let mut character = HashMap::new();
                add_to_character(&mut character, animation_name, frame_name, index);
                tmp.insert(character_name, character);
            }
        }
    }

    for (character_name, character) in tmp {
        let mut character_animations = CharacterAnimationCatalogue::default();
        for (animation_name, animation) in character {
            let mut builder = AnimationBuilder::new();
            builder.repeat(animation::Repeat::Forever);
            for (_frame_name, frame_index) in animation {
                builder.add_key_frame(
                    100,
                    TextureAtlasSprite3D {
                        index: frame_index,
                        ..Default::default()
                    },
                    None,
                    None,
                );
            }
            let animation = builder.build();
            let pos = animation.new_pos();
            let handle = animations.add(animation);
            character_animations
                .animations
                .insert(animation_name, (handle, pos));
        }
        animation_catalogue
            .characters
            .insert(character_name, character_animations);
    }
}

#[derive(Resource, Default)]
struct AnimationSpriteHandles {
    handles: Vec<HandleUntyped>,
}

#[derive(Resource, Default)]
struct Selected {
    x: u16,
    y: u16,
}

#[derive(Debug, Component)]
struct MainCamera;

const SCROLL_BORDER: f32 = 2.0;

fn input(
    keyboard_input: Res<Input<KeyCode>>,
    mut wheel_input: EventReader<MouseWheel>,
    mut camera: Query<(&mut Transform, &mut Projection), With<MainCamera>>,
    mut selected: ResMut<Selected>,
    map: Res<TerrainMap>,
    mut tiles: Query<(&mut TerrainHeight, &mut TerrainTexture)>,
    time: Res<Time>,
    mut menu_state: ResMut<NextState<GameMenuState>>,
    windows: Query<&Window, With<PrimaryWindow>>,
) {
    let window = windows.single();
    let speed = 20.0 * time.delta_seconds();
    let zoom_scale = 0.1;

    let cursor_pos = window.cursor_position();
    let size = Vec2::new(window.width(), window.height());

    if keyboard_input.just_pressed(KeyCode::Escape) {
        menu_state.set(GameMenuState::Open);
        return;
    }

    if let Ok((mut transform, mut projection)) = camera.get_single_mut() {
        if keyboard_input.pressed(KeyCode::Right) {
            transform.translation.x += speed;
        }
        if keyboard_input.pressed(KeyCode::Left) {
            transform.translation.x -= speed;
        }
        if keyboard_input.pressed(KeyCode::Up) {
            transform.translation.z -= speed;
        }
        if keyboard_input.pressed(KeyCode::Down) {
            transform.translation.z += speed;
        }

        if let Some(pos) = cursor_pos {
            if pos.x < SCROLL_BORDER {
                transform.translation.x -= speed;
            }
            if pos.y < SCROLL_BORDER {
                transform.translation.y -= speed;
            }
            if pos.x >= size.x - SCROLL_BORDER {
                transform.translation.x += speed;
            }
            if pos.y >= size.y - SCROLL_BORDER {
                transform.translation.y += speed;
            }
        }

        if let Projection::Orthographic(p) = projection.as_mut() {
            if !wheel_input.is_empty() {
                let offset = wheel_input
                    .iter()
                    .fold(1.0, |acc, ev| acc - ev.y * zoom_scale);

                p.scale = (p.scale * offset).clamp(0.0001, 0.2);
            }
        }
    } else {
        error!("No Main Camera found!");
    }

    if keyboard_input.just_pressed(KeyCode::A) {
        selected.x = selected.x.saturating_sub(1);
    }
    if keyboard_input.just_pressed(KeyCode::S) {
        selected.y += 1;
    }
    if keyboard_input.just_pressed(KeyCode::D) {
        selected.x += 1;
    }
    if keyboard_input.just_pressed(KeyCode::W) {
        selected.y = selected.y.saturating_sub(1);
    }

    if keyboard_input.just_pressed(KeyCode::C) {
        let Some(entity)= map.get(selected.x, selected.y)else {return};
        let Ok((_, mut texture)) = tiles.get_mut(entity) else {return;};

        texture.0 += 1;
        texture.0 %= 4;
    }
    if keyboard_input.just_pressed(KeyCode::Period) {
        let Some(entity)= map.get(selected.x, selected.y)else {return};
        let Ok((mut height, _)) = tiles.get_mut(entity) else {return;};
        height.0 += 1;
    }
    if keyboard_input.just_pressed(KeyCode::Comma) {
        let Some(entity)= map.get(selected.x, selected.y)else {return};
        let Ok((mut height, _)) = tiles.get_mut(entity) else {return;};
        height.0 = height.0.saturating_sub(1);
    }
}

#[derive(Debug, Resource, Default)]
struct ColonistAtlas(Handle<TextureAtlas>);

#[derive(Debug, Component)]
struct Carrier;

fn setup_colonists(
    asset_server: ResMut<AssetServer>,
    animation_sprite_handles: Res<AnimationSpriteHandles>,
    mut textures: ResMut<Assets<Image>>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut colonist_atlas: ResMut<ColonistAtlas>,
) {
    let mut texture_atlas_builder = TextureAtlasBuilder::default();
    for handle in &animation_sprite_handles.handles {
        let handle = handle.typed_weak();
        let Some(texture) = textures.get(&handle) else {
            warn!("{:?} did not resolve to an `Image` asset.", asset_server.get_handle_path(handle));
            continue;
        };

        texture_atlas_builder.add_texture(handle, texture);
    }

    let atlas = texture_atlas_builder.finish(&mut textures).unwrap();
    let atlas_handle = texture_atlases.add(atlas);

    colonist_atlas.0 = atlas_handle;
}

fn spawn_colonist(
    mut commands: Commands,
    colonists_atlas: Res<ColonistAtlas>,
    terrain_map: Res<TerrainMap>,
    mut writer: EventWriter<PathSegmentStarted>,
) {
    let entities: Vec<_> = [
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 1),
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 1),
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 1),
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 1),
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 1),
    ]
    .iter()
    .filter_map(|(x, y)| terrain_map.get(*x, *y))
    .collect();

    let segments = entities
        .windows(2)
        .map(|slice| pathing::PathSegment {
            from: slice[0],
            to: slice[1],
        })
        .collect();

    let path = Path::new(segments);
    let current = path.current_segment();
    let entity = commands
        .spawn((
            SpriteSheet3DBundle {
                transform: Transform::from_xyz(0.0, 0.0, 0.0),
                texture_atlas: colonists_atlas.0.clone(),
                ..Default::default()
            },
            path,
            Carrier,
            Gender::Female,
        ))
        .id();

    if let Some(current) = current {
        writer.send(PathSegmentStarted(entity, current))
    }
}

#[derive(Debug, Component)]
enum Gender {
    _Male,
    Female,
}

fn update_carrier_animation(
    mut event_reader: EventReader<PathSegmentStarted>,
    animation_catalogue: Res<AnimationCatalogue>,
    carrier: Query<&Gender, With<Carrier>>,
    mut commands: Commands,
) {
    for event in event_reader.iter() {
        let Ok(gender) = carrier.get(event.0)else {
            continue;
        };

        let animation = match gender {
            Gender::_Male => animation_catalogue.get_animation("male", "walk"),
            Gender::Female => animation_catalogue.get_animation("female", "walk"),
        };

        if let Some(animation) = animation {
            commands.entity(event.0).insert(animation);
        }
    }
}

fn setup(mut commands: Commands) {
    let transform = Transform::from_rotation(Quat::from_rotation_x(-0.7068583));

    commands.spawn((
        Camera3dBundle {
            projection: Projection::Orthographic(OrthographicProjection {
                scale: 0.01,
                far: 100.0,
                near: -1000.0,
                ..Default::default()
            }),
            transform,
            ..default()
        },
        MainCamera,
    ));
}

fn update_pathing_position(
    mut items: Query<(&pathing::Path, &mut Transform)>,
    tiles: Query<(&TerrainPosition, &TerrainHeight)>,
) {
    for (path, mut transform) in items.iter_mut() {
        let Some((entity_from, entity_to, s)) = path.current() else {
            continue;
        };
        let Ok((pos_from, height_from)) = tiles.get(entity_from) else {
            continue;
        };
        let Ok((pos_to, height_to)) = tiles.get(entity_to) else {
            continue;
        };

        let from_pos = get_world_position(pos_from.x, pos_from.y);
        let from_height = get_world_height(height_from.0);
        let to_pos = get_world_position(pos_to.x, pos_to.y);
        let to_height = get_world_height(height_to.0);

        let from = Vec3::new(from_pos.x, from_height, from_pos.y);
        let to = Vec3::new(to_pos.x, to_height, to_pos.y);

        let pos = from.lerp(to, s);

        transform.translation = pos;
    }
}
