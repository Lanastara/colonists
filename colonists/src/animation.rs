use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    marker::PhantomData,
};

use bevy::{prelude::*, reflect::TypeUuid};
#[derive(Debug)]
pub struct AnimationPlugin<TData, TEvent>(PhantomData<(TData, TEvent)>);

impl<TData, TEvent> Default for AnimationPlugin<TData, TEvent> {
    fn default() -> Self {
        Self(Default::default())
    }
}

impl<
        TData: 'static + Clone + Hash + Send + Sync + TypeUuid + Component,
        TEvent: 'static + Clone + Hash + Send + Sync + TypeUuid,
    > Plugin for AnimationPlugin<TData, TEvent>
{
    fn build(&self, app: &mut App) {
        app.add_asset::<Animation<TData, TEvent>>();
    }
}

pub fn update_animations<TData, TEvent>(
    mut query: Query<(
        &Handle<Animation<TData, TEvent>>,
        &mut AnimationPosition,
        &mut TData,
    )>,
    time: Res<Time>,
    animations: Res<Assets<Animation<TData, TEvent>>>,
) where
    TData: 'static + Clone + Hash + Send + Sync + TypeUuid + Component,
    TEvent: 'static + Clone + Hash + Send + Sync + TypeUuid,
{
    for (anim, mut pos, mut data) in query.iter_mut() {
        let Some(animation) = animations.get(anim) else {
            warn!("could not update animation!");
            continue;
        };
        animation.advance_by(&mut pos, time.delta().as_millis() as u32);
        let Some(new_data) = animation.current(&pos) else {
            continue;
        };

        *data = new_data;
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AnimationEvent<TEvent> {
    AnimationFinished,
    Event(TEvent),
}

#[derive(Debug, Component, Clone)]
pub struct AnimationPosition {
    animation_hash: u64,
    repeat: u32,
    position: u32,
    looped: bool,
}

impl AnimationPosition {
    fn advance_by(&mut self, d_time_ms: u32, duration_ms: u32) {
        self.position += d_time_ms;
        let advance_repeat = self.position / duration_ms;
        self.repeat += advance_repeat;
        self.looped |= self.repeat != 0;
        self.position -= advance_repeat * duration_ms;
    }
}

#[derive(Debug, Component, TypeUuid)]
#[uuid = "95c97b9c-ac9f-4877-9f83-7fb0119d00ec"]
pub struct Animation<TData, TEvent> {
    segments: Vec<KeyFrame<TData, TEvent>>,
    duration_ms: u32,
    repeat: Repeat,
    hash: u64,
}

impl<TData: Hash, TEvent: Hash> Hash for Animation<TData, TEvent> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.segments.hash(state);
        self.duration_ms.hash(state);
        self.repeat.hash(state);
    }
}

#[derive(Debug, Hash)]
pub enum Repeat {
    Times(u32),
    Forever,
}

impl<TData, TEvent> Animation<TData, TEvent>
where
    TEvent: Clone + Hash,
    TData: Clone + Hash,
{
    fn advance_by(
        &self,
        position: &mut AnimationPosition,
        d_time_ms: u32,
    ) -> Vec<AnimationEvent<TEvent>> {
        let mut ret = Vec::new();

        if !self.check_pos(position) {
            return ret;
        }

        let old_repeat = position.repeat;
        let old_pos = position.position;
        let old_index = self
            .segments
            .binary_search_by_key(&old_pos, |frame| frame.start_ms)
            .unwrap_or_else(|e| e - 1);

        match self.repeat {
            Repeat::Times(t) if old_repeat > t => {
                return ret;
            }
            _ => (),
        }

        let at_start =
            position.repeat == 0 && position.position == 0 && d_time_ms != 0 && !position.looped;

        if at_start {
            if let Some(frame) = self.segments.get(0) {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
        }
        position.advance_by(d_time_ms, self.duration_ms);

        let repeat = position.repeat;
        let pos = position.position;
        let index = self
            .segments
            .binary_search_by_key(&pos, |frame| frame.start_ms)
            .unwrap_or_else(|e| e - 1);

        if repeat == old_repeat {
            if index == old_index {
                return ret;
            }

            if let Some(frame) = self.segments.get(old_index) {
                if let Some(ref event) = frame.end_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
            for frame in &self.segments[old_index + 1..index] {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
                if let Some(ref event) = frame.end_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
            if let Some(frame) = self.segments.get(index) {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
        } else {
            let repeat = match self.repeat {
                Repeat::Times(max_ret) => max_ret.min(repeat),
                Repeat::Forever => repeat,
            };

            if let Some(frame) = self.segments.get(old_index) {
                if let Some(ref event) = frame.end_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
            for frame in &self.segments[old_index + 1..] {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
                if let Some(ref event) = frame.end_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }

            for _r in old_repeat..repeat - 1 {
                for frame in &self.segments[..] {
                    if let Some(ref event) = frame.start_event {
                        ret.push(AnimationEvent::Event(event.clone()));
                    }
                    if let Some(ref event) = frame.end_event {
                        ret.push(AnimationEvent::Event(event.clone()));
                    }
                }
            }

            for frame in &self.segments[..index] {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
                if let Some(ref event) = frame.end_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
            if let Some(frame) = self.segments.get(index) {
                if let Some(ref event) = frame.start_event {
                    ret.push(AnimationEvent::Event(event.clone()));
                }
            }
        }

        if let Repeat::Times(max_repeat) = self.repeat {
            if repeat > max_repeat {
                if let Some(frame) = self.segments.last() {
                    if let Some(ref event) = frame.end_event {
                        ret.push(AnimationEvent::Event(event.clone()));
                    }
                }
                ret.push(AnimationEvent::AnimationFinished);
            }
        }
        ret
    }

    fn check_pos(&self, pos: &AnimationPosition) -> bool {
        self.hash == pos.animation_hash
    }

    pub fn current(&self, position: &AnimationPosition) -> Option<TData> {
        if !self.check_pos(position) {
            return None;
        }

        let repeat = position.repeat;
        match self.repeat {
            Repeat::Times(t) if repeat > t => {
                return None;
            }
            _ => (),
        }

        let pos = position.position;
        let index = self
            .segments
            .binary_search_by_key(&pos, |frame| frame.start_ms)
            .unwrap_or_else(|e| e - 1);

        self.segments
            .get(index)
            .map(|keyframe| keyframe.data.clone())
    }

    pub fn new_pos(&self) -> AnimationPosition {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        let hash = hasher.finish();
        AnimationPosition {
            animation_hash: hash,
            repeat: 0,
            position: 0,
            looped: false,
        }
    }
}

pub struct AnimationBuilder<TData, TEvent> {
    segments: Vec<KeyFrame<TData, TEvent>>,
    duration_ms: u32,
    repeat: Repeat,
}

impl<TData, TEvent> AnimationBuilder<TData, TEvent>
where
    TData: Hash,
    TEvent: Hash,
{
    pub fn new() -> Self {
        Self {
            segments: Vec::new(),
            duration_ms: 0,
            repeat: Repeat::Times(1),
        }
    }

    pub fn repeat(&mut self, repeat: Repeat) -> &mut Self {
        self.repeat = repeat;
        self
    }

    pub fn add_key_frame(
        &mut self,
        duration_ms: u32,
        data: TData,
        start_event: impl Into<Option<TEvent>>,
        end_event: impl Into<Option<TEvent>>,
    ) -> &mut Self {
        self.segments.push(KeyFrame {
            data,
            start_ms: self.duration_ms,
            start_event: start_event.into(),
            end_event: end_event.into(),
        });
        self.duration_ms += duration_ms;
        self
    }

    pub fn build(self) -> Animation<TData, TEvent> {
        let mut anim = Animation {
            segments: self.segments,
            duration_ms: self.duration_ms,
            repeat: self.repeat,
            hash: 0,
        };

        let mut hasher = DefaultHasher::new();
        anim.hash(&mut hasher);
        let hash = hasher.finish();
        anim.hash = hash;

        anim
    }
}

#[derive(Debug, Hash)]
pub struct KeyFrame<TData, TEvent> {
    data: TData,
    start_ms: u32,
    start_event: Option<TEvent>,
    end_event: Option<TEvent>,
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_sample_forever_animation() -> Animation<u32, u32> {
        let mut builder = AnimationBuilder::new();

        builder
            .repeat(Repeat::Times(2))
            .add_key_frame(100, 0, 0, 1)
            .add_key_frame(100, 1, 2, 3)
            .add_key_frame(100, 2, 4, 5);

        builder.build()
    }

    #[test]
    fn test_forever() {
        let anim = get_sample_forever_animation();
        let mut pos = anim.new_pos();

        let events = anim.advance_by(&mut pos, 50);
        assert_eq!(events, vec![AnimationEvent::Event(0)]);
        let events = anim.advance_by(&mut pos, 200);
        assert_eq!(
            events,
            vec![
                AnimationEvent::Event(1),
                AnimationEvent::Event(2),
                AnimationEvent::Event(3),
                AnimationEvent::Event(4),
            ]
        );
        let events = anim.advance_by(&mut pos, 50);
        assert_eq!(
            events,
            vec![AnimationEvent::Event(5), AnimationEvent::Event(0)]
        );
        let events = anim.advance_by(&mut pos, 400);
        assert_eq!(
            events,
            vec![
                AnimationEvent::Event(1),
                AnimationEvent::Event(2),
                AnimationEvent::Event(3),
                AnimationEvent::Event(4),
                AnimationEvent::Event(5),
                AnimationEvent::Event(0),
                AnimationEvent::Event(1),
                AnimationEvent::Event(2),
            ]
        );
        let events = anim.advance_by(&mut pos, 4_000);
        assert_eq!(
            events,
            vec![
                AnimationEvent::Event(3),
                AnimationEvent::Event(4),
                AnimationEvent::Event(5),
                AnimationEvent::Event(0),
                AnimationEvent::Event(1),
                AnimationEvent::Event(2),
                AnimationEvent::Event(3),
                AnimationEvent::Event(4),
                AnimationEvent::Event(5),
                AnimationEvent::AnimationFinished,
            ]
        );
    }
}
