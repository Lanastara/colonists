use std::{cmp::Ordering, collections::BTreeMap};

use bevy::{
    core_pipeline::core_3d::Transparent3d,
    ecs::system::{
        lifetimeless::{Read, SRes},
        SystemParamItem,
    },
    math::Vec3A,
    pbr::{MeshPipelineKey, SetMeshViewBindGroup},
    prelude::*,
    reflect::TypeUuid,
    render::{
        mesh::GpuBufferInfo,
        primitives::Aabb,
        render_asset::RenderAssets,
        render_phase::{
            AddRenderCommand, DrawFunctions, PhaseItem, RenderCommand, RenderCommandResult,
            RenderPhase, SetItemPipeline, TrackedRenderPass,
        },
        render_resource::{
            BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout,
            BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingResource, BlendState, Buffer,
            BufferDescriptor, BufferUsages, PipelineCache, SamplerBindingType, ShaderStages,
            SpecializedMeshPipeline, SpecializedMeshPipelines, TextureSampleType,
            TextureViewDimension, VertexAttribute, VertexBufferLayout, VertexStepMode,
        },
        view::ExtractedView,
        Extract, RenderApp, RenderSet,
    },
};
use bevy_custom_render::prelude::{RenderDevice, RenderQueue, VertexFormat};
use bytemuck::{Pod, Zeroable};

pub struct Sprite3DPlugin;

impl Plugin for Sprite3DPlugin {
    fn build(&self, app: &mut App) {
        let quad_handle = {
            let quad = shape::Quad::new(Vec2::ONE);
            let mut mesh = Mesh::from(quad);
            mesh.remove_attribute(Mesh::ATTRIBUTE_NORMAL);
            mesh.remove_attribute(Mesh::ATTRIBUTE_UV_0);
            let mut meshes = app.world.get_resource_mut::<Assets<Mesh>>().unwrap();
            QuadMesh(meshes.add(mesh))
        };
        app.add_system(calculate_sprite_aabb);
        app.sub_app_mut(RenderApp)
            .insert_resource(quad_handle)
            .add_render_command::<Transparent3d, DrawInstanced>()
            .init_resource::<InstanceTextureBindGroupLayout>()
            .init_resource::<Sprite3dPipeline>()
            .init_resource::<SpecializedMeshPipelines<Sprite3dPipeline>>()
            .add_system(extract_sprite3d.in_schedule(ExtractSchedule))
            .add_system(queue_custom.in_set(RenderSet::Queue))
            .add_system(prepare_instance_texture.in_set(RenderSet::PrepareFlush));
    }
}

fn calculate_sprite_aabb(mut query: Query<(&mut Aabb, &Transform), With<TextureAtlasSprite3D>>) {
    for (mut aabb, transform) in query.iter_mut() {
        let half_scale = transform.scale / 2.0;
        aabb.half_extents = half_scale.into();
        let center = Vec3A::new(0.0, half_scale.y, 0.0);
        aabb.center = center;
    }
}

#[derive(Resource)]
pub struct Sprite3dPipeline {
    shader: Handle<Shader>,
    mesh_pipeline: bevy::pbr::MeshPipeline,
    bind_group_data: BindGroupLayout,
}

impl SpecializedMeshPipeline for Sprite3dPipeline {
    type Key = bevy::pbr::MeshPipelineKey;

    fn specialize(
        &self,
        key: Self::Key,
        layout: &bevy::render::mesh::MeshVertexBufferLayout,
    ) -> Result<
        bevy::render::render_resource::RenderPipelineDescriptor,
        bevy::render::render_resource::SpecializedMeshPipelineError,
    > {
        let mut descriptor = self.mesh_pipeline.specialize(key, layout)?;

        descriptor.vertex.shader = self.shader.clone();

        descriptor.layout.pop();
        descriptor.layout.push(self.bind_group_data.clone());

        descriptor.vertex.buffers.push(VertexBufferLayout {
            array_stride: std::mem::size_of::<InstanceBufferEntry>() as u64,
            step_mode: VertexStepMode::Instance,
            attributes: vec![
                VertexAttribute {
                    format: VertexFormat::Float32x3,
                    offset: 0,
                    shader_location: 3,
                },
                VertexAttribute {
                    format: VertexFormat::Float32x2,
                    offset: VertexFormat::Float32x3.size(),
                    shader_location: 4,
                },
                VertexAttribute {
                    format: VertexFormat::Float32x2,
                    offset: VertexFormat::Float32x3.size() + VertexFormat::Float32x2.size(),
                    shader_location: 5,
                },
                VertexAttribute {
                    format: VertexFormat::Float32x2,
                    offset: VertexFormat::Float32x3.size() + VertexFormat::Float32x2.size() * 2,
                    shader_location: 6,
                },
                VertexAttribute {
                    format: VertexFormat::Float32x4,
                    offset: VertexFormat::Float32x3.size() + VertexFormat::Float32x2.size() * 2,
                    shader_location: 7,
                },
            ],
        });

        if let Some(fragment) = descriptor.fragment.as_mut() {
            fragment.shader = self.shader.clone();
            for target in &mut fragment.targets {
                if let Some(target) = target.as_mut() {
                    target.blend = Some(BlendState::ALPHA_BLENDING)
                }
            }
        }

        Ok(descriptor)
    }
}

impl FromWorld for Sprite3dPipeline {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.resource::<AssetServer>();
        let mesh_pipeline = world.resource::<bevy::pbr::MeshPipeline>();
        let shader = asset_server.load("shaders/instancing.wgsl");
        let bind_group_data = world.resource::<InstanceTextureBindGroupLayout>().0.clone();

        Self {
            shader,
            mesh_pipeline: mesh_pipeline.clone(),
            bind_group_data,
        }
    }
}

#[derive(Debug, Resource)]
struct InstanceTextureBindGroupLayout(BindGroupLayout);

impl FromWorld for InstanceTextureBindGroupLayout {
    fn from_world(world: &mut World) -> Self {
        let device = world.resource::<RenderDevice>();
        let bind_group = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("InstanceTextureBindgroup"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::FRAGMENT,
                    ty: bevy::render::render_resource::BindingType::Texture {
                        sample_type: TextureSampleType::Float { filterable: true },
                        view_dimension: TextureViewDimension::D2,
                        multisampled: false,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::FRAGMENT,
                    ty: bevy::render::render_resource::BindingType::Sampler(
                        SamplerBindingType::Filtering,
                    ),
                    count: None,
                },
            ],
        });
        Self(bind_group)
    }
}

#[derive(Debug, Component, Clone, TypeUuid)]
#[uuid = "a214e4cd-7533-477a-bea2-ccd1f2c35484"]
pub struct TextureAtlasSprite3D {
    pub color: Color,
    pub flip_x: bool,
    pub flip_y: bool,
    pub index: usize,
}

impl std::hash::Hash for TextureAtlasSprite3D {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.color.as_rgba_u32().hash(state);
        self.flip_x.hash(state);
        self.flip_y.hash(state);
        self.index.hash(state);
    }
}

#[derive(Debug, Default)]
struct BufferCache {
    buffer: Vec<Buffer>,
}

const INSTANCE_BUFFER_COUNT: usize = 1_000;

fn extract_sprite3d(
    mut commands: Commands,
    texture_atlases: Extract<Res<Assets<TextureAtlas>>>,
    atlas_query: Extract<
        Query<(
            &ComputedVisibility,
            &TextureAtlasSprite3D,
            &GlobalTransform,
            &Handle<TextureAtlas>,
        )>,
    >,
    render_device: Res<RenderDevice>,
    render_queue: Res<RenderQueue>,
    mut cache: Local<BufferCache>,
) {
    let mut sprites = Vec::new();
    for (visibility, atlas_sprite, transform, texture_atlas_handle) in atlas_query.iter() {
        if !visibility.is_visible() {
            continue;
        }
        if let Some(texture_atlas) = texture_atlases.get(texture_atlas_handle) {
            let rect = Some(
                *texture_atlas
                    .textures
                    .get(atlas_sprite.index)
                    .unwrap_or_else(|| {
                        panic!(
                            "Sprite index {:?} does not exist for texture atlas handle {:?}.",
                            atlas_sprite.index,
                            texture_atlas_handle.id(),
                        )
                    }),
            );

            let (scale, _rot, trans) = transform.to_scale_rotation_translation();
            let uv_min = rect
                .map(|r| r.min / texture_atlas.size)
                .unwrap_or(Vec2::ZERO);
            let uv_max = rect
                .map(|r| r.max / texture_atlas.size)
                .unwrap_or(Vec2::ONE);
            let (uv_min, uv_max) = match (atlas_sprite.flip_x, atlas_sprite.flip_y) {
                (true, true) => todo!(),
                (true, false) => todo!(),
                (false, true) => (uv_min, uv_max),
                (false, false) => (Vec2::new(uv_min.x, uv_max.y), Vec2::new(uv_max.x, uv_min.y)),
            };
            sprites.push((
                texture_atlas.texture.clone(),
                InstanceBufferEntry {
                    color: atlas_sprite.color.as_rgba_f32(),
                    pos: trans,
                    scale: scale.truncate(),
                    uv_min,
                    uv_max,
                },
            ));
        }
    }

    let mut instance_data: BTreeMap<_, Vec<InstanceBufferEntry>> = BTreeMap::new();
    for (id, entry) in sprites {
        if let Some(data) = instance_data.get_mut(&id) {
            let index = match data.binary_search_by(|i| {
                if entry.pos.z >= i.pos.z {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            }) {
                Ok(i) => i,
                Err(i) => i,
            };
            data.insert(index, entry);
        } else {
            instance_data.insert(id, vec![entry]);
        }
    }

    let mut index = 0;
    for (key, data) in instance_data {
        for data in data.chunks(INSTANCE_BUFFER_COUNT) {
            let buffer = if let Some(buffer) = cache.buffer.get(index) {
                render_queue.write_buffer(buffer, 0, bytemuck::cast_slice(data));
                buffer.clone()
            } else {
                let buffer = render_device.create_buffer(&BufferDescriptor {
                    label: Some("instance data buffer"),
                    size: (std::mem::size_of::<InstanceBufferEntry>() * INSTANCE_BUFFER_COUNT)
                        as u64,
                    usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
                    mapped_at_creation: false,
                });
                render_queue.write_buffer(&buffer, 0, bytemuck::cast_slice(data));
                cache.buffer.push(buffer.clone());

                buffer
            };

            commands.spawn((
                InstanceBuffer {
                    buffer,
                    length: data.len(),
                },
                key.clone(),
            ));
            index += 1
        }
    }
}

impl Default for TextureAtlasSprite3D {
    fn default() -> Self {
        Self {
            index: 0,
            color: Color::WHITE,
            flip_x: false,
            flip_y: false,
        }
    }
}

#[derive(Debug, Resource)]
struct QuadMesh(Handle<Mesh>);

fn prepare_instance_texture(
    mut commands: Commands,
    instances: Query<(Entity, &Handle<Image>), With<InstanceBuffer>>,
    images: Res<RenderAssets<Image>>,
    device: Res<RenderDevice>,
    bind_group: Res<InstanceTextureBindGroupLayout>,
) {
    for (entity, image_handle) in instances.iter() {
        let Some(image) = images.get(image_handle) else {
            continue;
        };

        commands.entity(entity).insert(InstanceTextureBindGroup {
            bind_group: device.create_bind_group(&BindGroupDescriptor {
                label: Some("InstanceTextureBindGroup"),
                layout: &bind_group.0,
                entries: &[
                    BindGroupEntry {
                        binding: 0,
                        resource: BindingResource::TextureView(&image.texture_view),
                    },
                    BindGroupEntry {
                        binding: 1,
                        resource: BindingResource::Sampler(&image.sampler),
                    },
                ],
            }),
        });
    }
}

fn queue_custom(
    transparent_3d_draw_functions: Res<DrawFunctions<Transparent3d>>,
    custom_pipeline: Res<Sprite3dPipeline>,
    msaa: Res<Msaa>,
    mut pipelines: ResMut<SpecializedMeshPipelines<Sprite3dPipeline>>,
    pipeline_cache: Res<PipelineCache>,
    meshes: Res<RenderAssets<Mesh>>,
    mut views: Query<(&ExtractedView, &mut RenderPhase<Transparent3d>)>,
    quad_mesh: Res<QuadMesh>,
    instances: Query<Entity, With<InstanceBuffer>>,
) {
    let draw_custom = transparent_3d_draw_functions.read().id::<DrawInstanced>();

    let msaa_key = MeshPipelineKey::from_msaa_samples(msaa.samples());

    let QuadMesh(ref mesh_handle) = quad_mesh.as_ref();
    if let Some(mesh) = meshes.get(mesh_handle) {
        for (view, mut transparent_phase) in &mut views {
            let view_key = msaa_key | MeshPipelineKey::from_hdr(view.hdr);
            let key = view_key | MeshPipelineKey::from_primitive_topology(mesh.primitive_topology);
            let pipeline = pipelines
                .specialize(&pipeline_cache, &custom_pipeline, key, &mesh.layout)
                .unwrap();
            for entity in instances.iter() {
                transparent_phase.add(Transparent3d {
                    entity,
                    pipeline,
                    draw_function: draw_custom,
                    distance: 0.0,
                });
            }
        }
    }
}

#[derive(Component)]
pub struct InstanceBuffer {
    buffer: Buffer,
    length: usize,
}

#[derive(Debug, Zeroable, Clone, Copy, Pod)]
#[repr(C)]
struct InstanceBufferEntry {
    pos: Vec3,
    uv_min: Vec2,
    uv_max: Vec2,
    scale: Vec2,
    color: [f32; 4],
}

type DrawInstanced = (
    SetItemPipeline,
    SetMeshViewBindGroup<0>,
    SetInstanceTexture<1>,
    SetInstancedBuffer,
    DrawMeshInstanced,
);

struct SetInstancedBuffer;
impl<P: PhaseItem> RenderCommand<P> for SetInstancedBuffer {
    type Param = ();
    type ViewWorldQuery = ();
    type ItemWorldQuery = Read<InstanceBuffer>;

    #[inline]
    fn render<'w>(
        _item: &P,
        _view: (),
        instance_buffer: &'w InstanceBuffer,
        _meshes: SystemParamItem<'w, '_, Self::Param>,
        pass: &mut TrackedRenderPass<'w>,
    ) -> RenderCommandResult {
        pass.set_vertex_buffer(1, instance_buffer.buffer.slice(..));

        RenderCommandResult::Success
    }
}

#[derive(Debug, Component)]
struct InstanceTextureBindGroup {
    bind_group: BindGroup,
}

struct SetInstanceTexture<const N: usize>;
impl<P: PhaseItem, const N: usize> RenderCommand<P> for SetInstanceTexture<N> {
    type Param = ();
    type ViewWorldQuery = ();
    type ItemWorldQuery = Read<InstanceTextureBindGroup>;

    #[inline]
    fn render<'w>(
        _item: &P,
        _view: (),
        item: &'w InstanceTextureBindGroup,
        _params: (),
        pass: &mut TrackedRenderPass<'w>,
    ) -> RenderCommandResult {
        pass.set_bind_group(N, &item.bind_group, &[]);
        RenderCommandResult::Success
    }
}

struct DrawMeshInstanced;
impl<P: PhaseItem> RenderCommand<P> for DrawMeshInstanced {
    type Param = (SRes<QuadMesh>, SRes<RenderAssets<Mesh>>);
    type ViewWorldQuery = ();
    type ItemWorldQuery = Read<InstanceBuffer>;

    #[inline]
    fn render<'w>(
        _item: &P,
        _view: (),
        instance_buffer: &InstanceBuffer,
        meshes: SystemParamItem<'w, '_, Self::Param>,
        pass: &mut TrackedRenderPass<'w>,
    ) -> RenderCommandResult {
        let Some(gpu_mesh) = meshes.1.into_inner().get(&meshes.0.0) else {
            return RenderCommandResult::Failure;
        };

        pass.set_vertex_buffer(0, gpu_mesh.vertex_buffer.slice(..));

        match &gpu_mesh.buffer_info {
            GpuBufferInfo::Indexed {
                buffer,
                index_format,
                count,
            } => {
                pass.set_index_buffer(buffer.slice(..), 0, *index_format);
                pass.draw_indexed(0..*count, 0, 0..instance_buffer.length as u32);
            }
            GpuBufferInfo::NonIndexed { vertex_count } => {
                pass.draw(0..*vertex_count, 0..instance_buffer.length as u32);
            }
        }

        RenderCommandResult::Success
    }
}

#[derive(Bundle, Default)]
pub struct SpriteSheet3DBundle {
    pub transform: Transform,
    pub global_transform: GlobalTransform,
    pub visibility: Visibility,
    pub computed_visibility: ComputedVisibility,
    pub aabb: Aabb,
    pub texture_atlas: Handle<TextureAtlas>,
    pub sprite: TextureAtlasSprite3D,
}
