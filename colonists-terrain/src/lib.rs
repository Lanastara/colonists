#![allow(clippy::type_complexity)]
#![allow(clippy::too_many_arguments)]
use std::collections::BTreeMap;

use bevy::{
    asset::{load_internal_asset, LoadState},
    core_pipeline::core_3d::Opaque3d,
    prelude::*,
    reflect::TypeUuid,
    render::{
        mesh::VertexAttributeDescriptor,
        primitives::Aabb,
        render_resource::{
            AddressMode, AsBindGroup, FilterMode, SamplerDescriptor, SpecializedMeshPipeline,
        },
        texture::ImageSampler,
    },
};
use bevy_custom_render::prelude::*;
use bytemuck::cast_slice;
use prelude::TerrainTextureLoader;
use textures::TerrainTexture;

mod textures;

#[cfg(test)]
mod test;

pub mod prelude {
    pub use super::textures::TerrainTexture;
    pub use super::{textures::TerrainTextureLoader, TerrainHeight, TerrainPlugin};
}

pub struct TerrainPlugin {
    pub chunk: Option<(u16, u16)>,
    pub world: (u16, u16),
}

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
enum TerrainSet {
    Create,
    Maps,
    Update,
}

impl Plugin for TerrainPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        load_internal_asset!(app, TERRAIN_SHADER_HANDLE, "shader.wgsl", Shader::from_wgsl);

        app.add_plugin(RawMeshPlugin::<
            TerrainPipeline<TerrainMaterial>,
            TerrainMaterial,
            Opaque3d,
        >::default())
            .insert_resource(WorldSettings {
                size: self.world,
                chunk: self.chunk.unwrap_or(self.world),
            })
            .init_resource::<TerrainMap>()
            .init_resource::<ChunkMap>()
            .configure_sets((
                TerrainSet::Create.before(TerrainSet::Maps),
                TerrainSet::Maps.before(TerrainSet::Update),
            ))
            .add_system(load_textures)
            .add_systems(
                (
                    manage_tiles.run_if(resource_changed::<WorldSettings>()),
                    manage_chunks.run_if(
                        resource_exists::<TerrainMaterialHandle>()
                            .and_then(resource_changed::<WorldSettings>()),
                    ),
                )
                    .in_set(TerrainSet::Create)
                    .in_base_set(CoreSet::PreUpdate),
            )
            .add_systems(
                (update_tilemap, update_chunkmap)
                    .in_set(TerrainSet::Maps)
                    .in_base_set(CoreSet::PreUpdate),
            )
            .add_system(
                update_chunks
                    .in_set(TerrainSet::Update)
                    .in_base_set(CoreSet::PreUpdate),
            );
    }
}

#[derive(AsBindGroup, TypeUuid, Debug, Clone)]
#[uuid = "f690fdae-d598-45ab-8225-97e2a3f056e0"]
pub struct TerrainMaterial {
    #[texture(0, dimension = "2d_array")]
    #[sampler(1)]
    textures: Option<Handle<Image>>,
    #[texture(2)]
    #[sampler(3)]
    blend_map: Option<Handle<Image>>,
}

impl Material for TerrainMaterial {}

#[derive(Resource)]
struct TerrainPipeline<M: Material> {
    shader: Handle<Shader>,
    material_pipeline: bevy::pbr::MaterialPipeline<M>,
}

impl<M: Material> FromWorld for TerrainPipeline<M> {
    fn from_world(world: &mut World) -> Self {
        let shader = TERRAIN_SHADER_HANDLE.typed::<Shader>();

        let mesh_pipeline = world.resource::<bevy::pbr::MaterialPipeline<M>>();

        TerrainPipeline {
            shader,
            material_pipeline: mesh_pipeline.clone(),
        }
    }
}

impl<M: Material> SpecializedMeshPipeline for TerrainPipeline<M>
where
    M::Data: Eq + core::hash::Hash + Copy,
{
    type Key = bevy::pbr::MaterialPipelineKey<M>;

    fn specialize(
        &self,
        key: Self::Key,
        layout: &bevy::render::mesh::MeshVertexBufferLayout,
    ) -> Result<
        bevy::render::render_resource::RenderPipelineDescriptor,
        bevy::render::render_resource::SpecializedMeshPipelineError,
    > {
        let mut descriptor = self.material_pipeline.specialize(key, layout)?;
        descriptor.vertex.shader = self.shader.clone();

        descriptor.vertex.buffers.clear();
        let mut descriptors = Vec::new();
        for (loc, a) in TerrainTile::get_layout().into_iter().enumerate() {
            descriptors.push(VertexAttributeDescriptor::new(
                loc.try_into().unwrap(),
                a.id,
                a.name,
            ));
        }
        descriptor
            .vertex
            .buffers
            .push(layout.get_layout(&descriptors[..])?);
        descriptor.fragment.as_mut().unwrap().shader = self.shader.clone();
        if let Some(mut depth) = descriptor.depth_stencil.as_mut() {
            depth.depth_write_enabled = false;
        }
        Ok(descriptor)
    }
}

const TERRAIN_SHADER_HANDLE: HandleUntyped =
    HandleUntyped::weak_from_u64(Shader::TYPE_UUID, 8674999279812608);

#[derive(Debug, Resource, Default)]
pub struct TerrainMap {
    entities: BTreeMap<(u16, u16), Entity>,
}

impl TerrainMap {
    pub fn get(&self, x: u16, y: u16) -> Option<Entity> {
        self.entities.get(&(x, y)).cloned()
    }
}

#[derive(Debug, Resource, Default)]
struct ChunkMap {
    entities: BTreeMap<(u16, u16), Entity>,
}

#[derive(Debug, Component)]
pub struct TerrainHeight(pub u8);

#[derive(Debug, Component)]
pub struct TerrainPosition {
    pub x: u16,
    pub y: u16,
}

#[derive(Component)]
pub struct ChunkPosition {
    x: u16,
    y: u16,
}

#[derive(Component)]
pub struct ChunkSize {
    width: u16,
    _height: u16,
}

#[derive(Debug, Resource)]
pub struct WorldSettings {
    size: (u16, u16),
    chunk: (u16, u16),
}

#[derive(Debug, Resource)]
pub struct TerrainMaterialHandle(Handle<TerrainMaterial>);

fn load_textures(
    mut commands: Commands,
    mut materials: ResMut<Assets<TerrainMaterial>>,
    asset_server: Res<AssetServer>,
    mut loading_texture: ResMut<TerrainTextureLoader>,
    mut images: ResMut<Assets<Image>>,
) {
    if loading_texture.loaded
        || asset_server.get_load_state(loading_texture.atlas.clone()) != LoadState::Loaded
        || asset_server.get_load_state(loading_texture.blend_texture.clone()) != LoadState::Loaded
    {
        return;
    }
    loading_texture.loaded = true;
    let image = images.get_mut(&loading_texture.atlas).unwrap();
    image.reinterpret_stacked_2d_as_array(loading_texture.size);
    image.sampler_descriptor = ImageSampler::Descriptor(SamplerDescriptor {
        address_mode_u: AddressMode::Repeat,
        address_mode_v: AddressMode::Repeat,
        mag_filter: FilterMode::Linear,
        min_filter: FilterMode::Linear,
        ..Default::default()
    });

    let image = images.get_mut(&loading_texture.blend_texture).unwrap();
    image.sampler_descriptor = ImageSampler::Descriptor(SamplerDescriptor {
        address_mode_u: AddressMode::ClampToEdge,
        address_mode_v: AddressMode::ClampToEdge,
        mag_filter: FilterMode::Nearest,
        min_filter: FilterMode::Nearest,
        ..Default::default()
    });

    let material = materials.add(TerrainMaterial {
        textures: Some(loading_texture.atlas.clone()),
        blend_map: Some(loading_texture.blend_texture.clone()),
    });

    commands.insert_resource(TerrainMaterialHandle(material));
}

#[derive(Debug, Component)]
struct Deleted;

fn manage_tiles(mut commands: Commands, settings: Res<WorldSettings>, tiles: Res<TerrainMap>) {
    for ((x, y), entity) in &tiles.entities {
        if *x >= settings.size.0 || *y >= settings.size.1 {
            commands.entity(*entity).insert(Deleted);
        }
    }

    for y in 0..settings.size.1 {
        for x in 0..settings.size.0 {
            if tiles.get(x, y).is_none() {
                commands.spawn((
                    TerrainHeight(0),
                    TerrainPosition { x, y },
                    TerrainTexture(0),
                ));
            }
        }
    }
}

fn update_tilemap(
    tiles: Query<
        (Entity, &TerrainPosition, Option<&Deleted>),
        Or<(Added<TerrainPosition>, Added<Deleted>)>,
    >,
    mut tilemap: ResMut<TerrainMap>,
) {
    for (entity, tile, deleted) in tiles.iter() {
        if deleted.is_some() {
            tilemap.entities.remove(&(tile.x, tile.y));
        } else if let Some(tile) = tilemap.entities.get_mut(&(tile.x, tile.y)) {
            *tile = entity;
        } else {
            tilemap.entities.insert((tile.x, tile.y), entity);
        }
    }
}

#[derive(Debug, Pod, Clone, Copy, Zeroable, Default)]
#[repr(C)]
struct TerrainTile {
    uv: Vec2,
    height: f32,
    blend: [u8; 4],
    normal: Vec3,
}

impl BufferObject for TerrainTile {
    fn get_layout() -> Vec<MeshVertexAttribute> {
        vec![
            MeshVertexAttribute::new("Uv", 999_999, VertexFormat::Float32x2),
            MeshVertexAttribute::new("Height", 1_000_000, VertexFormat::Float32),
            MeshVertexAttribute::new("Blend", 1_000_001, VertexFormat::Uint32),
            MeshVertexAttribute::new("Normal", 1_000_002, VertexFormat::Float32x3),
        ]
    }
}

impl UpdateBufferObject for TerrainTile {
    type Update = TerrainTileUpdate;
}

#[derive(Debug, Default)]
struct TerrainTileUpdate {
    uv: Option<Vec2>,
    height: Option<f32>,
    blend: Option<[u8; 4]>,
    normal: Option<Vec3>,
}

impl BufferObjectUpdate for TerrainTileUpdate {
    fn get_updates(&self) -> Vec<(Vec<u8>, u64)> {
        let mut ret: Vec<(Vec<u8>, u64)> = Vec::new();
        if let Some(uv) = self.uv {
            ret.push((cast_slice::<_, u8>(&[uv]).to_vec(), 0))
        }
        if let Some(height) = self.height {
            ret.push((cast_slice::<_, u8>(&[height]).to_vec(), 8))
        }
        if let Some(blend) = self.blend {
            ret.push((cast_slice::<_, u8>(&[blend]).to_vec(), 12))
        }
        if let Some(normal) = self.normal {
            ret.push((cast_slice::<_, u8>(&[normal]).to_vec(), 16))
        }
        ret
    }
}

fn create_mesh(
    chunk_x: u16,
    chunk_y: u16,
    settings: &WorldSettings,
) -> (Vec<TerrainTile>, Aabb, (u16, u16)) {
    let x_start = chunk_x * (settings.chunk.0 - 1);
    let y_start = chunk_y * (settings.chunk.1 - 1);
    let x_end = (x_start + settings.chunk.0).min(settings.size.0);
    let y_end = (y_start + settings.chunk.1).min(settings.size.1);

    let width = x_end - x_start;
    let height = y_end - y_start;

    let mut min = Vec3::new(f32::MAX, 0.0, f32::MAX);
    let mut max = Vec3::ZERO;

    let mut data = Vec::new();

    for y in y_start..=y_end {
        for x in x_start..=x_end {
            let uv_x = x as f32;
            let uv_y = y as f32;
            data.push(TerrainTile {
                uv: Vec2::new(uv_x, uv_y),
                normal: Vec3::Y,
                ..Default::default()
            });

            let Vec2 { x, y } = get_world_position(x, y);

            if x < min.x {
                min.x = x;
            }
            if y < min.z {
                min.z = y;
            }
            if x > max.x {
                max.x = x;
            }
            if y > max.z {
                max.z = y;
            }
        }
    }
    (data, Aabb::from_min_max(min, max), (width, height))
}

fn create_indices(chunk_x: u16, chunk_y: u16, settings: &WorldSettings) -> Vec<u32> {
    let x_start = chunk_x * (settings.chunk.0 - 1);
    let y_start = chunk_y * (settings.chunk.1 - 1);
    let x_end = (x_start + settings.chunk.0).min(settings.size.0);
    let y_end = (y_start + settings.chunk.1).min(settings.size.1);
    let w = u32::from(x_end - x_start) + 1;

    (y_start..y_end)
        .flat_map(|y| {
            let y = u32::from(y - y_start);
            (x_start..x_end).flat_map(move |x| {
                let x = u32::from(x - x_start);
                [
                    w * y + x + 1,
                    w * y + x,
                    w * (y + 1) + x,
                    w * y + x + 1,
                    w * (y + 1) + x,
                    w * (y + 1) + x + 1,
                ]
            })
        })
        .collect()
}

fn manage_chunks(
    mut commands: Commands,
    settings: Res<WorldSettings>,
    chunks: Res<ChunkMap>,
    device: Res<RenderDevice>,
    handle: Res<TerrainMaterialHandle>,
) {
    let width = ((settings.size.0 as f32 / (settings.chunk.0 - 1) as f32) + 0.5) as u16;
    let height = ((settings.size.1 as f32 / (settings.chunk.1 - 1) as f32) + 0.5) as u16;

    for ((x, y), entity) in &chunks.entities {
        if *x >= width || *y >= height {
            commands.entity(*entity).insert(Deleted);
        }
    }

    for y in 0..height {
        for x in 0..width {
            if chunks.entities.get(&(x, y)).is_none() {
                let indices = create_indices(x, y, &settings);
                let (mesh, aabb, (width, _height)) = create_mesh(x, y, &settings);
                let mesh = RawMeshHelper::<TerrainTile>::make_mesh(
                    mesh,
                    PrimitiveTopology::TriangleList,
                    Some(Indices::U32(indices)),
                    &device,
                );

                commands.spawn((
                    ChunkSize { width, _height },
                    aabb,
                    ChunkPosition { x, y },
                    MaterialRawMeshBundle::<TerrainPipeline<TerrainMaterial>> {
                        mesh,
                        ..Default::default()
                    },
                    handle.0.clone(),
                ));
            }
        }
    }
}

fn update_chunkmap(
    tiles: Query<
        (Entity, &ChunkPosition, Option<&Deleted>),
        Or<(Added<ChunkPosition>, Added<Deleted>)>,
    >,
    mut tilemap: ResMut<ChunkMap>,
) {
    for (entity, tile, deleted) in tiles.iter() {
        if deleted.is_some() {
            tilemap.entities.remove(&(tile.x, tile.y));
        } else if let Some(tile) = tilemap.entities.get_mut(&(tile.x, tile.y)) {
            *tile = entity;
        } else {
            tilemap.entities.insert((tile.x, tile.y), entity);
        }
    }
}

fn get_chunk_positions(x: u16, y: u16, settings: &WorldSettings) -> Vec<((u16, u16), (u16, u16))> {
    let chunk_width = settings.chunk.0 - 1;
    let chunk_height = settings.chunk.1 - 1;
    let chunk_index_x = x / chunk_width;
    let chunk_index_y = y / chunk_height;

    let inner_x = x - (chunk_index_x * chunk_width);
    let inner_y = y - (chunk_index_y * chunk_height);

    let mut ret = vec![((chunk_index_x, chunk_index_y), (inner_x, inner_y))];
    if inner_x == 0 && chunk_index_x != 0 {
        ret.push(((chunk_index_x - 1, chunk_index_y), (chunk_width, inner_y)));
        if inner_y == 0 && chunk_index_y != 0 {
            ret.push(((chunk_index_x, chunk_index_y - 1), (inner_x, chunk_height)));
            ret.push((
                (chunk_index_x - 1, chunk_index_y - 1),
                (chunk_width, chunk_height),
            ));
        }
    } else if inner_y == 0 && chunk_index_y != 0 {
        ret.push(((chunk_index_x, chunk_index_y - 1), (inner_x, chunk_height)));
    }
    ret
}

fn update_chunks(
    changed_tiles: Query<
        (&TerrainPosition, Ref<TerrainHeight>, Ref<TerrainTexture>),
        Or<(Changed<TerrainHeight>, Changed<TerrainTexture>)>,
    >,
    tiles: Query<(&TerrainTexture, &TerrainHeight)>,
    chunk_map: Res<ChunkMap>,
    tile_map: Res<TerrainMap>,
    mut chunks: Query<(&mut RawMesh, &ChunkSize)>,
    settings: Res<WorldSettings>,
    queue: Res<RenderQueue>,
) {
    for (pos, height, texture) in changed_tiles.iter() {
        let chunk_positions = get_chunk_positions(pos.x, pos.y, &settings);
        for (chunk, position) in chunk_positions {
            //TODO: update only changed properties of mesh vertex

            {
                let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                let index = position.1 * (size.width + 1) + position.0;
                let mut update = TerrainTileUpdate::default();
                if texture.is_changed() {
                    let textures = get_tile_textures((pos.x, pos.y), &tile_map, &tiles);
                    update.blend = Some(textures);
                }

                if height.is_changed() {
                    update.height = Some(get_world_height(height.0));
                }
                RawMeshHelper::<TerrainTile>::update_element(
                    &queue,
                    &mut mesh,
                    index as u32,
                    &update,
                );
            }

            if height.is_changed() {
                let coord = grid_coords::triangle::vertex::Vertex::new(pos.x, pos.y);
                let neigbors = coord.checked_adjacent();

                for neigbor in neigbors {
                    let Some(neigbor ) = neigbor else {
                        continue;
                    };
                    let neigbors = neigbor.checked_adjacent();
                    let normal = get_vertex_normal(&tile_map, pos, &tiles, neigbors);

                    let chunk_positions = get_chunk_positions(neigbor.q(), neigbor.r(), &settings);
                    for (chunk, position) in chunk_positions {
                        let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                        let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                        let index = position.1 * (size.width + 1) + position.0;
                        let update = TerrainTileUpdate {
                            normal: Some(normal),
                            ..Default::default()
                        };
                        RawMeshHelper::<TerrainTile>::update_element(
                            &queue,
                            &mut mesh,
                            index as u32,
                            &update,
                        );
                    }
                }

                let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                let index = position.1 * (size.width + 1) + position.0;
                let normal = get_vertex_normal(&tile_map, pos, &tiles, neigbors);
                let update = TerrainTileUpdate {
                    normal: Some(normal),
                    ..Default::default()
                };
                RawMeshHelper::<TerrainTile>::update_element(
                    &queue,
                    &mut mesh,
                    index as u32,
                    &update,
                );
            }

            if texture.is_changed() {
                if let Some(pos) = pos.y.checked_sub(1).map(|y| (pos.x, y)) {
                    let textures = get_tile_textures(pos, &tile_map, &tiles);
                    let update = TerrainTileUpdate {
                        blend: Some(textures),
                        ..Default::default()
                    };

                    let chunk_positions = get_chunk_positions(pos.0, pos.1, &settings);
                    for (chunk, position) in chunk_positions {
                        let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                        let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                        let index = position.1 * (size.width + 1) + position.0;

                        RawMeshHelper::<TerrainTile>::update_element(
                            &queue,
                            &mut mesh,
                            index as u32,
                            &update,
                        );
                    }
                }

                if let Some(pos) = pos.y.checked_sub(1).map(|y| (pos.x + 1, y)) {
                    let textures = get_tile_textures(pos, &tile_map, &tiles);
                    let update = TerrainTileUpdate {
                        blend: Some(textures),
                        ..Default::default()
                    };

                    let chunk_positions = get_chunk_positions(pos.0, pos.1, &settings);
                    for (chunk, position) in chunk_positions {
                        let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                        let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                        let index = position.1 * (size.width + 1) + position.0;

                        RawMeshHelper::<TerrainTile>::update_element(
                            &queue,
                            &mut mesh,
                            index as u32,
                            &update,
                        );
                    }
                }

                {
                    let pos = (pos.x + 1, pos.y);

                    let textures = get_tile_textures(pos, &tile_map, &tiles);
                    let update = TerrainTileUpdate {
                        blend: Some(textures),
                        ..Default::default()
                    };

                    let chunk_positions = get_chunk_positions(pos.0, pos.1, &settings);
                    for (chunk, position) in chunk_positions {
                        let Some(chunk_entity) = chunk_map.entities.get(&chunk) else {continue;};
                        let Ok((mut mesh, size)) = chunks.get_mut(*chunk_entity) else {continue;};
                        let index = position.1 * (size.width + 1) + position.0;

                        RawMeshHelper::<TerrainTile>::update_element(
                            &queue,
                            &mut mesh,
                            index as u32,
                            &update,
                        );
                    }
                }
            }
        }
    }
}

fn get_vertex_normal(
    tile_map: &Res<TerrainMap>,
    pos: &TerrainPosition,
    tiles: &Query<(&TerrainTexture, &TerrainHeight)>,
    neigbors: [Option<grid_coords::triangle::vertex::Vertex<u16>>; 6],
) -> Vec3 {
    let Some(pos) = get_tile_position(tile_map, pos.x, pos.y, tiles) else {
        return Vec3::ZERO;
    };
    let neigbor_vec: Vec<Option<Vec3>> = neigbors
        .iter()
        .map(|coord| {
            coord.and_then(|coord| {
                get_tile_position(tile_map, coord.q(), coord.r(), tiles).map(|n_pos| pos - n_pos)
            })
        })
        .collect();

    let mut sum_normal = Vec3::ZERO;

    for i in 0..6 {
        let vec1 = neigbor_vec[i];
        let vec2 = neigbor_vec[(i + 1) % 6];

        let (Some(vec1), Some(vec2)) = (vec1, vec2) else {
            sum_normal += Vec3::Y;
            continue;
        };

        sum_normal += vec1.cross(vec2);
    }

    sum_normal.normalize_or_zero()
}

fn get_tile_position(
    tile_map: &TerrainMap,
    x: u16,
    y: u16,
    tiles: &Query<(&TerrainTexture, &TerrainHeight)>,
) -> Option<Vec3> {
    tile_map
        .get(x, y)
        .and_then(|entity| tiles.get(entity).ok())
        .map(|texture| texture.1 .0)
        .map(|h| (get_world_position(x, y), get_world_height(h)))
        .map(|(v, y)| Vec3::new(v.x, y, v.y))
}

fn get_tile_textures(
    pos: (u16, u16),
    tile_map: &Res<TerrainMap>,
    tiles: &Query<(&TerrainTexture, &TerrainHeight)>,
) -> [u8; 4] {
    let tl = (pos.0.saturating_sub(1), pos.1);
    let tb1 = (pos.0.saturating_sub(1), pos.1 + 1);
    let tb2 = (pos.0, pos.1 + 1);

    let t = tile_map
        .get(pos.0, pos.1)
        .and_then(|entity| tiles.get(entity).ok())
        .map(|texture| texture.0 .0)
        .unwrap_or_default();
    let tl = tile_map
        .get(tl.0, tl.1)
        .and_then(|entity| tiles.get(entity).ok())
        .map(|texture| texture.0 .0)
        .unwrap_or_default();
    let tb1 = tile_map
        .get(tb1.0, tb1.1)
        .and_then(|entity| tiles.get(entity).ok())
        .map(|texture| texture.0 .0)
        .unwrap_or_default();
    let tb2 = tile_map
        .get(tb2.0, tb2.1)
        .and_then(|entity| tiles.get(entity).ok())
        .map(|texture| texture.0 .0)
        .unwrap_or_default();
    [tb2, tb1, t, tl]
}

pub fn get_world_height(height: u8) -> f32 {
    0.2 * height as f32
}

pub fn get_world_position(x: u16, y: u16) -> Vec2 {
    let x = f32::from(x);
    let y = f32::from(y);
    let y_stride = (3.0_f32.sqrt()) / 2.0;
    let x = x + y * 0.5;
    let y = y * y_stride;
    Vec2::new(x, y)
}
