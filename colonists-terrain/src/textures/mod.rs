use bevy::prelude::{Component, Handle, Image, Resource};

#[derive(Resource)]
pub struct TerrainTextureLoader {
    pub(crate) atlas: Handle<Image>,
    pub(crate) blend_texture: Handle<Image>,
    pub(crate) loaded: bool,
    pub(crate) size: u32,
}

impl TerrainTextureLoader {
    pub fn new(atlas: Handle<Image>, blend_texture: Handle<Image>, size: u32) -> Self {
        Self {
            atlas,
            loaded: false,
            size,
            blend_texture,
        }
    }
}

impl PartialEq for TerrainTextureLoader {
    fn eq(&self, other: &Self) -> bool {
        self.loaded == other.loaded
    }
}

#[derive(Debug, Component)]
pub struct TerrainTexture(pub u8);
