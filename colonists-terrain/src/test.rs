use super::*;

#[test]
fn test_get_chunk_positions() {
    let settings = WorldSettings {
        size: (10, 10),
        chunk: (3, 3),
    };

    assert_eq!(vec![((2, 2), (1, 1))], get_chunk_positions(5, 5, &settings));
    assert_eq!(
        vec![
            ((2, 2), (0, 0)),
            ((1, 2), (2, 0)),
            ((2, 1), (0, 2)),
            ((1, 1), (2, 2))
        ],
        get_chunk_positions(4, 4, &settings)
    );
    assert_eq!(
        vec![((2, 2), (0, 1)), ((1, 2), (2, 1)),],
        get_chunk_positions(4, 5, &settings)
    );
    assert_eq!(
        vec![((2, 2), (1, 0)), ((2, 1), (1, 2)),],
        get_chunk_positions(5, 4, &settings)
    );
    assert_eq!(vec![((1, 1), (1, 1))], get_chunk_positions(3, 3, &settings));
}
