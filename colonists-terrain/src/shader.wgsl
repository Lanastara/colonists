
#import bevy_pbr::mesh_view_bindings
#import bevy_pbr::mesh_bindings

@group(1) @binding(0)
var my_array_texture: texture_2d_array<f32>;
@group(1) @binding(1)
var my_array_texture_sampler: sampler;
@group(1) @binding(2)
var blend_texture: texture_2d<f32>;
@group(1) @binding(3)
var blend_texture_sampler: sampler;

// NOTE: Bindings must come before functions that use them!
#import bevy_pbr::mesh_functions

struct Vertex {
    @location(0) Uv: vec2<f32>,
    @location(1) Height: f32,
    @location(2) Blend: u32,
    @location(3) Normal: vec3<f32>
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) @interpolate(flat) blend_color: vec4<i32>,
    @location(1) uv: vec2<f32>,
    @location(2) @interpolate(flat) uv_anchor: vec2<f32>,
    @location(3) orig_pos: vec3<f32>,
    @location(4) normal : vec3<f32>,
};

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    var y_stride = sqrt(3.0) / 2.0;
    var out: VertexOutput;
    let pos = vec4<f32>(vertex.Uv.x + 0.5 * vertex.Uv.y, vertex.Height, vertex.Uv.y * y_stride, 1.0);
    out.clip_position = mesh_position_local_to_clip(mesh.model, pos);
    var blend = i32(vertex.Blend);
    out.blend_color = vec4<i32>((blend >> 24u) & 255, (blend >> 16u) & 255, (blend >> 8u) & 255, (blend >> 0u) & 255);
    out.uv = vertex.Uv;
    out.uv_anchor = vertex.Uv - vec2(1.0, 0.0);
    out.orig_pos = pos.xyz;
    out.normal = vertex.Normal;
    return out;
}

struct FragmentInput {
    @location(0) @interpolate(flat) blend_color: vec4<i32>,
    @location(1) uv: vec2<f32>,
    @location(2) @interpolate(flat) uv_anchor: vec2<f32>,
    @location(3) orig_pos: vec3<f32>,
    @location(4) normal : vec3<f32>,
};

@fragment
fn fragment(input: FragmentInput) -> @location(0) vec4<f32> {
    let light_vec = normalize(vec3(-1.0, 1.0, 1.0));
    
    let diffuse = max(dot(light_vec, input.normal), 0.0);
    
    let blend = textureSample(blend_texture, blend_texture_sampler, input.uv - input.uv_anchor);

    let color_0 = textureSample(my_array_texture, my_array_texture_sampler, input.uv, input.blend_color.x);
    let color_1 = textureSample(my_array_texture, my_array_texture_sampler, input.uv, input.blend_color.y);
    let color_2 = textureSample(my_array_texture, my_array_texture_sampler, input.uv, input.blend_color.z);
    let color_3 = textureSample(my_array_texture, my_array_texture_sampler, input.uv, input.blend_color.w);
    return vec4((color_0 * blend.x + color_1 * blend.y + color_2 * blend.z + color_3 * blend.w).rgb * diffuse, 1.0);
}
