#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Hex {
    q: i16,
    r: i16,
}

impl Hex {
    pub fn new(q: i16, r: i16) -> Self {
        Self { q, r }
    }

    pub fn q(&self) -> i16 {
        self.q
    }

    pub fn r(&self) -> i16 {
        self.r
    }

    // pub fn s(&self) -> i16 {
    //     -(self.q + self.r)
    // }

    pub fn neighbors(&self) -> Neighbors<Hex> {
        Neighbors {
            north_east: Hex {
                q: self.q + 1,
                r: self.r - 1,
            },
            east: Hex {
                q: self.q + 1,
                r: self.r,
            },
            south_east: Hex {
                q: self.q,
                r: self.r + 1,
            },
            south_west: Hex {
                q: self.q - 1,
                r: self.r + 1,
            },
            west: Hex {
                q: self.q - 1,
                r: self.r,
            },
            north_west: Hex {
                q: self.q,
                r: self.r - 1,
            },
        }
    }

    // pub fn position(&self, radius: f32) -> Vec2 {
    //     let width_2 = radius * f32::sqrt(3.0) / 2.0;
    //     let q = self.q as f32;
    //     let r = self.r as f32;

    //     let x = (q + r / 2.0) * 2.0 * width_2;
    //     let y = r * 2.0 * radius * 0.75;

    //     Vec2::new(x, y)
    // }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Neighbors<T> {
    pub north_east: T,
    pub east: T,
    pub south_east: T,
    pub south_west: T,
    pub west: T,
    pub north_west: T,
}

impl<T> Neighbors<T> {
    // pub fn to_array(self) -> [T; 6] {
    //     [
    //         self.north_east,
    //         self.east,
    //         self.south_east,
    //         self.south_west,
    //         self.west,
    //         self.north_west,
    //     ]
    // }

    pub fn map<TOut, TFn: Fn(T) -> TOut>(self, map: TFn) -> Neighbors<TOut> {
        Neighbors {
            north_east: map(self.north_east),
            east: map(self.east),
            south_east: map(self.south_east),
            south_west: map(self.south_west),
            west: map(self.west),
            north_west: map(self.north_west),
        }
    }
}

#[derive(Debug, Clone)]
pub struct MapInfo {
    pub chunk_size: i16,
    pub tile_radius: f32,
    pub size: (i16, i16),
    pub loop_x: bool,
    loop_y: bool,
}

impl MapInfo {
    pub fn new(
        chunk_size: u16,
        tile_radius: f32,
        size: (u16, u16),
        loop_x: bool,
        loop_y: bool,
    ) -> Self {
        Self {
            chunk_size: chunk_size as i16,
            tile_radius,
            size: (size.0 as i16, size.1 as i16),
            loop_x,
            loop_y,
        }
    }

    pub fn get_looping_coord(&self, coord: &Hex) -> Option<Hex> {
        let mut q = coord.q();
        let mut r = coord.r();

        if self.loop_x {
            while q < 0 {
                q += self.size.0;
            }
            while q >= self.size.0 {
                q -= self.size.0;
            }
        }

        if self.loop_y {
            while r < 0 {
                r += self.size.1;
            }
            while r >= self.size.1 {
                r -= self.size.1;
            }
        }

        if q >= 0 && q < self.size.0 && r >= 0 && r < self.size.1 {
            Some(Hex::new(q, r))
        } else {
            None
        }
    }
}