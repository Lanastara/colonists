use std::ops::{Add, Sub};

use super::{edge::Edge, vertex::Vertex};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Symbol {
    L,
    R,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Tile<TIndex> {
    q: TIndex,
    r: TIndex,
    s: Symbol,
}

impl<TIndex> Tile<TIndex>
where
    TIndex: Copy,
{
    pub fn new(q: TIndex, r: TIndex, s: Symbol) -> Self {
        Self { q, r, s }
    }

    pub fn neighbors(&self) -> [Tile<TIndex>; 3]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex>,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::L => [
                Tile::new(self.q, self.r, Symbol::R),
                Tile::new(self.q, self.r - one, Symbol::R),
                Tile::new(self.q - one, self.r, Symbol::R),
            ],
            Symbol::R => [
                Tile::new(self.q, self.r + one, Symbol::L),
                Tile::new(self.q + one, self.r, Symbol::L),
                Tile::new(self.q, self.r, Symbol::L),
            ],
        }
    }

    pub fn borders(&self) -> [Edge<TIndex>; 3]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex>,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::L => [
                Edge::new(self.q, self.r, super::edge::Symbol::E),
                Edge::new(self.q, self.r, super::edge::Symbol::N),
                Edge::new(self.q, self.r, super::edge::Symbol::W),
            ],
            Symbol::R => [
                Edge::new(self.q, self.r + one, super::edge::Symbol::N),
                Edge::new(self.q + one, self.r, super::edge::Symbol::W),
                Edge::new(self.q, self.r, super::edge::Symbol::E),
            ],
        }
    }

    pub fn corners(&self) -> [Vertex<TIndex>; 3]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex>,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::L => [
                Vertex::new(self.q, self.r + one),
                Vertex::new(self.q + one, self.r),
                Vertex::new(self.q, self.r),
            ],
            Symbol::R => [
                Vertex::new(self.q + one, self.r + one),
                Vertex::new(self.q + one, self.r),
                Vertex::new(self.q, self.r + one),
            ],
        }
    }

    pub fn q(&self) -> TIndex {
        self.q
    }

    pub fn r(&self) -> TIndex {
        self.r
    }

    pub fn s(&self) -> Symbol {
        self.s
    }
}
