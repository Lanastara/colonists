use std::ops::{Add, Sub};

use super::{edge::Edge, tile::Tile};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Vertex<TIndex> {
    q: TIndex,
    r: TIndex,
}

impl<TIndex> Vertex<TIndex>
where
    TIndex: Copy,
{
    pub fn new(q: TIndex, r: TIndex) -> Self {
        Self { q, r }
    }

    pub fn saturating_touches(&self) -> [Tile<TIndex>; 6]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + num_traits::SaturatingSub,
    {
        let one = num_traits::One::one();
        use super::tile::Symbol;
        [
            Tile::new(self.q.saturating_sub(&one), self.r, Symbol::R),
            Tile::new(self.q, self.r, Symbol::L),
            Tile::new(self.q, self.r.saturating_sub(&one), Symbol::R),
            Tile::new(self.q, self.r.saturating_sub(&one), Symbol::L),
            Tile::new(
                self.q.saturating_sub(&one),
                self.r.saturating_sub(&one),
                Symbol::R,
            ),
            Tile::new(self.q.saturating_sub(&one), self.r, Symbol::L),
        ]
    }

    pub fn checked_touches(&self) -> [Option<Tile<TIndex>>; 6]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + num_traits::CheckedSub,
    {
        let one = num_traits::One::one();
        use super::tile::Symbol;
        [
            self.q
                .checked_sub(&one)
                .map(|q| Tile::new(q, self.r, Symbol::R)),
            Some(Tile::new(self.q, self.r, Symbol::L)),
            self.r
                .checked_sub(&one)
                .map(|r| Tile::new(self.q, r, Symbol::R)),
            self.r
                .checked_sub(&one)
                .map(|r| Tile::new(self.q, r, Symbol::L)),
            self.q
                .checked_sub(&one)
                .and_then(|q| self.r.checked_sub(&one).map(|r| Tile::new(q, r, Symbol::R))),
            self.q
                .checked_sub(&one)
                .map(|q| Tile::new(q, self.r, Symbol::L)),
        ]
    }

    pub fn protrudes(&self) -> [Edge<TIndex>; 6]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + num_traits::SaturatingSub,
    {
        let one = num_traits::One::one();
        use super::edge::Symbol;
        [
            Edge::new(self.q, self.r, Symbol::W),
            Edge::new(self.q, self.r, Symbol::N),
            Edge::new(self.q, self.r.saturating_sub(&one), Symbol::E),
            Edge::new(self.q, self.r.saturating_sub(&one), Symbol::W),
            Edge::new(self.q.saturating_sub(&one), self.r, Symbol::N),
            Edge::new(self.q.saturating_sub(&one), self.r, Symbol::E),
        ]
    }

    pub fn adjacent(&self) -> [Vertex<TIndex>; 6]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex>,
    {
        let one = num_traits::One::one();
        [
            Vertex::new(self.q, self.r + one),
            Vertex::new(self.q + one, self.r),
            Vertex::new(self.q + one, self.r - one),
            Vertex::new(self.q, self.r - one),
            Vertex::new(self.q - one, self.r),
            Vertex::new(self.q - one, self.r + one),
        ]
    }

    pub fn checked_adjacent(&self) -> [Option<Vertex<TIndex>>; 6]
    where
        TIndex: num_traits::One + Add<TIndex, Output = TIndex> + num_traits::CheckedSub,
    {
        let one = num_traits::One::one();
        [
            Some(Vertex::new(self.q, self.r + one)),
            Some(Vertex::new(self.q + one, self.r)),
            self.r
                .checked_sub(&one)
                .map(|r| Vertex::new(self.q + one, r)),
            self.r.checked_sub(&one).map(|r| Vertex::new(self.q, r)),
            self.q.checked_sub(&one).map(|q| Vertex::new(q, self.r)),
            self.q
                .checked_sub(&one)
                .map(|q| Vertex::new(q, self.r + one)),
        ]
    }

    pub fn distance(&self, other: &Self) -> TIndex
    where
        TIndex: num_traits::Signed + Add<TIndex, Output = TIndex>,
    {
        num_traits::abs_sub(self.q, other.q) + num_traits::abs_sub(self.r, other.r)
    }

    pub fn q(&self) -> TIndex {
        self.q
    }

    pub fn r(&self) -> TIndex {
        self.r
    }
}
