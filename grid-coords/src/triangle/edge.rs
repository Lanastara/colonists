use std::ops::{Add, Sub};

use super::{tile::Tile, vertex::Vertex};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Symbol {
    E,
    N,
    W,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Edge<TIndex> {
    q: TIndex,
    r: TIndex,
    s: Symbol,
}

impl<TIndex> Edge<TIndex> {
    pub fn new(q: TIndex, r: TIndex, s: Symbol) -> Self {
        Self { q, r, s }
    }

    pub fn joins(&self) -> [Tile<TIndex>; 2]
    where
        TIndex:
            num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex> + Copy,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::E => [
                Tile::new(self.q, self.r, super::tile::Symbol::R),
                Tile::new(self.q, self.r, super::tile::Symbol::L),
            ],
            Symbol::N => [
                Tile::new(self.q, self.r, super::tile::Symbol::L),
                Tile::new(self.q, self.r - one, super::tile::Symbol::R),
            ],
            Symbol::W => [
                Tile::new(self.q, self.r, super::tile::Symbol::L),
                Tile::new(self.q - one, self.r, super::tile::Symbol::R),
            ],
        }
    }

    pub fn continues(&self) -> [Edge<TIndex>; 2]
    where
        TIndex:
            num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex> + Copy,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::E => [
                Edge::new(self.q + one, self.r - one, Symbol::E),
                Edge::new(self.q - one, self.r + one, Symbol::E),
            ],
            Symbol::N => [
                Edge::new(self.q + one, self.r, Symbol::N),
                Edge::new(self.q - one, self.r, Symbol::N),
            ],
            Symbol::W => [
                Edge::new(self.q, self.r + one, Symbol::W),
                Edge::new(self.q, self.r - one, Symbol::W),
            ],
        }
    }

    pub fn endpoints(&self) -> [Vertex<TIndex>; 2]
    where
        TIndex:
            num_traits::One + Add<TIndex, Output = TIndex> + Sub<TIndex, Output = TIndex> + Copy,
    {
        let one = num_traits::One::one();
        match self.s {
            Symbol::E => [
                Vertex::new(self.q + one, self.r),
                Vertex::new(self.q, self.r + one),
            ],
            Symbol::N => [
                Vertex::new(self.q + one, self.r),
                Vertex::new(self.q, self.r),
            ],
            Symbol::W => [
                Vertex::new(self.q, self.r + one),
                Vertex::new(self.q, self.r),
            ],
        }
    }
}
