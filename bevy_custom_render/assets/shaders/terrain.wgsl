

// #import bevy_pbr::mesh_types
// #import bevy_pbr::mesh_view_bindings

// @group(1) @binding(0)
// var<uniform> mesh: Mesh;

// NOTE: Bindings must come before functions that use them!
// #import bevy_pbr::mesh_functions

struct Vertex {
    // @location(0) Vertex_Uv: vec2<f32>,
    // @location(1) Vertex_Height: f32,
    // @location(2) Vertex_Blend: vec4<u8>,
          
};
                        
struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) color: vec4<f32>,
};
                                    
@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    let position = vertex.position;
    var out: VertexOutput;
    out.clip_position = mesh_position_local_to_clip(mesh.model, vec4<f32>(position, 1.0));
    return out;
}
                                                            
@fragment
fn fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    return in.color;
}
