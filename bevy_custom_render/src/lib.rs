#![allow(clippy::type_complexity)]
#![allow(clippy::too_many_arguments)]
use std::marker::PhantomData;

use bevy::{
    core::{cast_slice, Pod},
    core_pipeline::core_3d::{AlphaMask3d, Opaque3d, Transparent3d},
    ecs::system::lifetimeless::Read,
    math::Mat3A,
    pbr::{
        MaterialPipelineKey, MeshPipelineKey, MeshUniform, NotShadowCaster, NotShadowReceiver,
        RenderMaterials, SetMaterialBindGroup, SetMeshBindGroup, SetMeshViewBindGroup,
    },
    prelude::*,
    render::{
        extract_component::{ExtractComponent, ExtractComponentPlugin},
        mesh::{GpuBufferInfo, GpuMesh, Indices, MeshVertexAttribute, VertexAttributeValues},
        render_phase::{
            AddRenderCommand, CachedRenderPipelinePhaseItem, DrawFunctionId, DrawFunctions,
            PhaseItem, RenderCommand, RenderCommandResult, RenderPhase, SetItemPipeline,
            TrackedRenderPass,
        },
        render_resource::{
            BufferInitDescriptor, BufferUsages, CachedRenderPipelineId, IndexFormat, PipelineCache,
            PrimitiveTopology, SpecializedMeshPipeline, SpecializedMeshPipelines, VertexFormat,
        },
        renderer::{RenderDevice, RenderQueue},
        view::ExtractedView,
        Extract, RenderApp, RenderSet,
    },
};

pub mod prelude {
    pub use super::{
        BufferObject, BufferObjectUpdate, MaterialRawMeshBundle, RawMesh, RawMeshHelper,
        RawMeshPlugin, RenderMarker, UpdateBufferObject,
    };
    pub use bevy::{
        core::{Pod, Zeroable},
        render::{
            mesh::{GpuMesh, Indices, MeshVertexAttribute},
            render_resource::{PrimitiveTopology, VertexFormat},
            renderer::{RenderDevice, RenderQueue},
        },
    };
}

#[derive(Debug)]
pub struct RawMeshPlugin<S, M, P = Transparent3d>(PhantomData<(S, M, P)>);

impl<S, M, P> Default for RawMeshPlugin<S, M, P> {
    fn default() -> Self {
        Self(Default::default())
    }
}

impl<S, M, P> Plugin for RawMeshPlugin<S, M, P>
where
    S: SpecializedMeshPipeline + Resource + FromWorld,
    S::Key: Send + Sync + From<MaterialPipelineKey<M>>,
    M: Material,
    M::Data: Eq + core::hash::Hash + Copy,
    P: RenderPass,
{
    fn build(&self, app: &mut App) {
        app.add_plugin(ExtractComponentPlugin::<RenderMarker<S>>::default())
            .add_plugin(MaterialPlugin::<M>::default());

        app.sub_app_mut(RenderApp)
            .add_system(extract_raw_meshes.in_schedule(ExtractSchedule))
            .add_render_command::<P, DrawRawMeshCommands<M>>()
            .init_resource::<S>()
            .init_resource::<SpecializedMeshPipelines<S>>()
            .add_system(queue_custom::<S, M, P>.in_set(RenderSet::Queue));
    }
}

fn extract_raw_meshes(
    mut commands: Commands,
    mut prev_caster_commands_len: Local<usize>,
    mut prev_not_caster_commands_len: Local<usize>,
    meshes_query: Extract<
        Query<(
            Entity,
            &ComputedVisibility,
            &GlobalTransform,
            &RawMesh,
            Option<With<NotShadowReceiver>>,
            Option<With<NotShadowCaster>>,
        )>,
    >,
) {
    let mut caster_commands = Vec::with_capacity(*prev_caster_commands_len);
    let mut not_caster_commands = Vec::with_capacity(*prev_not_caster_commands_len);
    let visible_meshes = meshes_query.iter().filter(|(_, vis, ..)| vis.is_visible());

    for (entity, _, transform, mesh, not_receiver, not_caster) in visible_meshes {
        let transform = transform.compute_matrix();
        let mut flags = if not_receiver.is_some() { 0 } else { 1 };
        if Mat3A::from_mat4(transform).determinant().is_sign_positive() {
            flags |= 1 << 31;
        }
        let uniform = MeshUniform {
            flags,
            transform,
            inverse_transpose_model: transform.inverse().transpose(),
        };
        if not_caster.is_some() {
            not_caster_commands.push((entity, (mesh.clone(), uniform, NotShadowCaster)));
        } else {
            caster_commands.push((entity, (mesh.clone(), uniform)));
        }
    }
    *prev_caster_commands_len = caster_commands.len();
    *prev_not_caster_commands_len = not_caster_commands.len();
    commands.insert_or_spawn_batch(caster_commands);
    commands.insert_or_spawn_batch(not_caster_commands);
}

#[derive(Debug, Component)]
pub struct RenderMarker<S: SpecializedMeshPipeline + Resource>(PhantomData<S>);

impl<S: SpecializedMeshPipeline + Resource> Default for RenderMarker<S> {
    fn default() -> Self {
        Self(Default::default())
    }
}

impl<S: SpecializedMeshPipeline + Resource> ExtractComponent for RenderMarker<S> {
    type Query = &'static RenderMarker<S>;

    type Filter = ();

    type Out = Self;

    fn extract_component(item: bevy::ecs::query::QueryItem<'_, Self::Query>) -> Option<Self::Out> {
        Some(Self(item.0))
    }
}

trait RenderPass: CachedRenderPipelinePhaseItem {
    fn new(
        distance: f32,
        pipeline: CachedRenderPipelineId,
        entity: Entity,
        draw_function: DrawFunctionId,
    ) -> Self;
}

impl RenderPass for Opaque3d {
    fn new(
        distance: f32,
        pipeline: CachedRenderPipelineId,
        entity: Entity,
        draw_function: DrawFunctionId,
    ) -> Self {
        Self {
            distance,
            pipeline,
            entity,
            draw_function,
        }
    }
}

impl RenderPass for Transparent3d {
    fn new(
        distance: f32,
        pipeline: CachedRenderPipelineId,
        entity: Entity,
        draw_function: DrawFunctionId,
    ) -> Self {
        Self {
            distance,
            pipeline,
            entity,
            draw_function,
        }
    }
}

impl RenderPass for AlphaMask3d {
    fn new(
        distance: f32,
        pipeline: CachedRenderPipelineId,
        entity: Entity,
        draw_function: DrawFunctionId,
    ) -> Self {
        Self {
            distance,
            pipeline,
            entity,
            draw_function,
        }
    }
}

fn queue_custom<S, M, P>(
    transparent_3d_draw_functions: Res<DrawFunctions<P>>,
    msaa: Res<Msaa>,
    mut views: Query<(&ExtractedView, &mut RenderPhase<P>)>,
    material_meshes: Query<(Entity, &MeshUniform, &RawMesh, &Handle<M>), With<RenderMarker<S>>>,
    pipeline_cache: Res<PipelineCache>,
    mut pipelines: ResMut<SpecializedMeshPipelines<S>>,
    custom_pipeline: Res<S>,
    render_materials: Res<RenderMaterials<M>>,
) where
    S: SpecializedMeshPipeline + Resource,
    S::Key: Send + Sync + From<MaterialPipelineKey<M>>,
    M: Material,
    M::Data: Clone,
    P: RenderPass,
{
    let draw_custom = transparent_3d_draw_functions
        .read()
        .id::<DrawRawMeshCommands<M>>();

    let msaa_key = MeshPipelineKey::from_msaa_samples(msaa.samples());

    for (view, mut transparent_phase) in &mut views {
        let view_key = msaa_key | MeshPipelineKey::from_hdr(view.hdr);
        let rangefinder = view.rangefinder3d();

        for (entity, mesh_uniform, mesh, material_handle) in &material_meshes {
            if let Some(material) = render_materials.get(material_handle) {
                if let Some(mesh) = &mesh.0 {
                    let mesh_key = view_key
                        | MeshPipelineKey::from_primitive_topology(mesh.primitive_topology);
                    let pipeline = pipelines
                        .specialize(
                            &pipeline_cache,
                            &custom_pipeline,
                            S::Key::from(MaterialPipelineKey {
                                mesh_key,
                                bind_group_data: material.key.clone(),
                            }),
                            &mesh.layout,
                        )
                        .unwrap();

                    transparent_phase.add(P::new(
                        rangefinder.distance(&mesh_uniform.transform),
                        pipeline,
                        entity,
                        draw_custom,
                    ))
                }
            }
        }
    }
}

#[derive(Bundle)]
pub struct MaterialRawMeshBundle<S: SpecializedMeshPipeline + Resource> {
    pub mesh: RawMesh,
    pub transform: Transform,
    pub global_transform: GlobalTransform,
    pub visibility: Visibility,
    pub computed_visibility: ComputedVisibility,
    pub marker: RenderMarker<S>,
}

impl<S: SpecializedMeshPipeline + Resource> Default for MaterialRawMeshBundle<S> {
    fn default() -> Self {
        Self {
            mesh: Default::default(),
            transform: Default::default(),
            global_transform: Default::default(),
            visibility: Default::default(),
            computed_visibility: Default::default(),
            marker: Default::default(),
        }
    }
}

type DrawRawMeshCommands<M> = (
    SetItemPipeline,
    SetMeshViewBindGroup<0>,
    SetMaterialBindGroup<M, 1>,
    SetMeshBindGroup<2>,
    DrawRawMesh,
);

#[derive(Debug, Component, Clone, Default)]
pub struct RawMesh(pub Option<GpuMesh>);

impl ExtractComponent for RawMesh {
    type Query = &'static RawMesh;

    type Filter = ();

    type Out = RawMesh;

    fn extract_component(item: bevy::ecs::query::QueryItem<'_, Self::Query>) -> Option<Self::Out> {
        Some(item.clone())
    }
}

pub trait BufferObject: Pod {
    fn get_layout() -> Vec<MeshVertexAttribute>;
}

pub trait UpdateBufferObject: BufferObject {
    type Update: BufferObjectUpdate;
}

pub trait BufferObjectUpdate {
    fn get_updates(&self) -> Vec<(Vec<u8>, u64)>;
}

pub struct RawMeshHelper<T>(PhantomData<T>);

impl<T: BufferObject> RawMeshHelper<T> {
    pub fn make_mesh(
        data: Vec<T>,
        primitive_topology: PrimitiveTopology,
        indices: Option<Indices>,
        device: &RenderDevice,
    ) -> RawMesh {
        let mut mesh = Mesh::new(primitive_topology);

        for a in T::get_layout() {
            let data = match a.format {
                VertexFormat::Uint8x2 => VertexAttributeValues::Uint8x2(vec![]),
                VertexFormat::Uint8x4 => VertexAttributeValues::Uint8x4(vec![]),
                VertexFormat::Sint8x2 => VertexAttributeValues::Sint8x2(vec![]),
                VertexFormat::Sint8x4 => VertexAttributeValues::Sint8x4(vec![]),
                VertexFormat::Unorm8x2 => VertexAttributeValues::Unorm8x2(vec![]),
                VertexFormat::Unorm8x4 => VertexAttributeValues::Unorm8x4(vec![]),
                VertexFormat::Snorm8x2 => VertexAttributeValues::Snorm8x2(vec![]),
                VertexFormat::Snorm8x4 => VertexAttributeValues::Snorm8x4(vec![]),
                VertexFormat::Uint16x2 => VertexAttributeValues::Uint16x2(vec![]),
                VertexFormat::Uint16x4 => VertexAttributeValues::Uint16x4(vec![]),
                VertexFormat::Sint16x2 => VertexAttributeValues::Sint16x2(vec![]),
                VertexFormat::Sint16x4 => VertexAttributeValues::Sint16x4(vec![]),
                VertexFormat::Unorm16x2 => VertexAttributeValues::Unorm16x2(vec![]),
                VertexFormat::Unorm16x4 => VertexAttributeValues::Unorm16x4(vec![]),
                VertexFormat::Snorm16x2 => VertexAttributeValues::Snorm16x2(vec![]),
                VertexFormat::Snorm16x4 => VertexAttributeValues::Snorm16x4(vec![]),
                VertexFormat::Float16x2 => todo!(),
                VertexFormat::Float16x4 => todo!(),
                VertexFormat::Float32 => VertexAttributeValues::Float32(vec![]),
                VertexFormat::Float32x2 => VertexAttributeValues::Float32x2(vec![]),
                VertexFormat::Float32x3 => VertexAttributeValues::Float32x3(vec![]),
                VertexFormat::Float32x4 => VertexAttributeValues::Float32x4(vec![]),
                VertexFormat::Uint32 => VertexAttributeValues::Uint32(vec![]),
                VertexFormat::Uint32x2 => VertexAttributeValues::Uint32x2(vec![]),
                VertexFormat::Uint32x3 => VertexAttributeValues::Uint32x3(vec![]),
                VertexFormat::Uint32x4 => VertexAttributeValues::Uint32x4(vec![]),
                VertexFormat::Sint32 => VertexAttributeValues::Sint32(vec![]),
                VertexFormat::Sint32x2 => VertexAttributeValues::Sint32x2(vec![]),
                VertexFormat::Sint32x3 => VertexAttributeValues::Sint32x3(vec![]),
                VertexFormat::Sint32x4 => VertexAttributeValues::Sint32x4(vec![]),
                VertexFormat::Float64 => todo!(),
                VertexFormat::Float64x2 => todo!(),
                VertexFormat::Float64x3 => todo!(),
                VertexFormat::Float64x4 => todo!(),
            };

            mesh.insert_attribute(a, data);
        }

        let vertex_buffer = device.create_buffer_with_data(&BufferInitDescriptor {
            label: None,
            contents: cast_slice(&data[..]),
            usage: BufferUsages::COPY_DST | BufferUsages::VERTEX,
        });

        let buffer_info = match indices {
            Some(indices) => {
                let (index_format, count, buffer) = match indices {
                    Indices::U16(v) => (
                        IndexFormat::Uint16,
                        v.len() as u32,
                        device.create_buffer_with_data(&BufferInitDescriptor {
                            label: None,
                            contents: cast_slice(&v[..]),
                            usage: BufferUsages::COPY_DST | BufferUsages::INDEX,
                        }),
                    ),
                    Indices::U32(v) => (
                        IndexFormat::Uint32,
                        v.len() as u32,
                        device.create_buffer_with_data(&BufferInitDescriptor {
                            label: None,
                            contents: cast_slice(&v[..]),
                            usage: BufferUsages::COPY_DST | BufferUsages::INDEX,
                        }),
                    ),
                };
                GpuBufferInfo::Indexed {
                    buffer,
                    count,
                    index_format,
                }
            }
            None => GpuBufferInfo::NonIndexed {
                vertex_count: data.len() as u32,
            },
        };

        RawMesh(Some(GpuMesh {
            vertex_buffer,
            buffer_info,
            primitive_topology,
            layout: mesh.get_mesh_vertex_buffer_layout(),
        }))
    }

    pub fn replace_element(query: &RenderQueue, mesh: &mut RawMesh, index: u32, element: T) {
        let Some(ref mesh) = mesh.0 else {return;};
        let offset = u64::from(index) * std::mem::size_of::<T>() as u64;
        let slice = [element];
        let data = cast_slice(&slice);
        query.0.write_buffer(&mesh.vertex_buffer, offset, data);
    }
}

impl<T: UpdateBufferObject> RawMeshHelper<T> {
    pub fn update_element(query: &RenderQueue, mesh: &mut RawMesh, index: u32, update: &T::Update) {
        let Some(ref mesh) = mesh.0 else {return;};
        let offset = u64::from(index) * std::mem::size_of::<T>() as u64;
        for (data, add_offset) in update.get_updates() {
            query
                .0
                .write_buffer(&mesh.vertex_buffer, offset + add_offset, &data[..]);
        }
    }
}
struct DrawRawMesh;

impl<P: PhaseItem> RenderCommand<P> for DrawRawMesh {
    type Param = ();

    type ViewWorldQuery = ();

    type ItemWorldQuery = Read<RawMesh>;

    fn render<'w>(
        _item: &P,
        _view: (),
        entity: &'w RawMesh,
        _param: (),
        pass: &mut TrackedRenderPass<'w>,
    ) -> bevy::render::render_phase::RenderCommandResult {
        let Some(gpu_mesh) = &entity.0 else {
            return RenderCommandResult::Failure;
        };
        pass.set_vertex_buffer(0, gpu_mesh.vertex_buffer.slice(..));

        match &gpu_mesh.buffer_info {
            GpuBufferInfo::Indexed {
                buffer,
                index_format,
                count,
            } => {
                pass.set_index_buffer(buffer.slice(..), 0, *index_format);
                pass.draw_indexed(0..*count, 0, 0..1);
            }
            GpuBufferInfo::NonIndexed { vertex_count } => {
                pass.draw(0..*vertex_count, 0..1);
            }
        }
        RenderCommandResult::Success
    }
}
